<?php /*%%SmartyHeaderCode:8960879335abe1d308b7ec6-69016377%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4f7c0f49c5135069df928b54761be4d96036b3b3' => 
    array (
      0 => '/home/autom2eh/new.toolspanda.com/themes/default-bootstrap/modules/blockcms/blockcms.tpl',
      1 => 1517239232,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8960879335abe1d308b7ec6-69016377',
  'variables' => 
  array (
    'block' => 0,
    'cms_titles' => 0,
    'cms_key' => 0,
    'cms_title' => 0,
    'cms_page' => 0,
    'link' => 0,
    'show_price_drop' => 0,
    'PS_CATALOG_MODE' => 0,
    'show_new_products' => 0,
    'show_best_sales' => 0,
    'display_stores_footer' => 0,
    'show_contact' => 0,
    'contact_url' => 0,
    'cmslinks' => 0,
    'cmslink' => 0,
    'show_sitemap' => 0,
    'footer_text' => 0,
    'display_poweredby' => 0,
  ),
  'has_nocache_code' => true,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5abe1d308e4a18_10253317',
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abe1d308e4a18_10253317')) {function content_5abe1d308e4a18_10253317($_smarty_tpl) {?>
	<!-- Block CMS module footer -->
	<section class="footer-block col-xs-12 col-sm-2" id="block_various_links_footer">
		<h4>Информация</h4>
		<ul class="toggle-footer">
							<li class="item">
					<a href="http://new.toolspanda.com/bg/prices-drop" title="Промоции">
						Промоции
					</a>
				</li>
									<li class="item">
				<a href="http://new.toolspanda.com/bg/new-products" title="Нови продукти">
					Нови продукти
				</a>
			</li>
										<li class="item">
					<a href="http://new.toolspanda.com/bg/best-sales" title="Най-продавани">
						Най-продавани
					</a>
				</li>
										<li class="item">
					<a href="http://new.toolspanda.com/bg/stores" title="Нашите магазини">
						Нашите магазини
					</a>
				</li>
									<li class="item">
				<a href="http://new.toolspanda.com/bg/contact-us" title="Контакти">
					Контакти
				</a>
			</li>
															<li class="item">
						<a href="http://new.toolspanda.com/bg/content/3-terms-and-conditions-of-use" title="Terms and conditions of use">
							Terms and conditions of use
						</a>
					</li>
																<li class="item">
						<a href="http://new.toolspanda.com/bg/content/4-about-us" title="About us">
							About us
						</a>
					</li>
													<li>
				<a href="http://new.toolspanda.com/bg/sitemap" title="Карта на сайта">
					Карта на сайта
				</a>
			</li>
					</ul>
		
	</section>
		<section class="bottom-footer col-xs-12">
		<div>
			<?php echo smartyTranslate(array('s'=>'[1] %3$s %2$s - Ecommerce software by %1$s [/1]','mod'=>'blockcms','sprintf'=>array('PrestaShop™',date('Y'),'©'),'tags'=>array('<a class="_blank" href="http://www.prestashop.com">')),$_smarty_tpl);?>

		</div>
	</section>
		<!-- /Block CMS module footer -->
<?php }} ?>
