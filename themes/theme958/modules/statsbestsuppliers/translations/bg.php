<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsbestsuppliers}theme958>statsbestsuppliers_b5f5c19c8729b639d4d2a256fcb01a10'] = 'Върнат празен запис';
$_MODULE['<{statsbestsuppliers}theme958>statsbestsuppliers_f5c493141bb4b2508c5938fd9353291a'] = 'Показва %1$s от %2$s ';
$_MODULE['<{statsbestsuppliers}theme958>statsbestsuppliers_49ee3087348e8d44e1feda1917443987'] = 'Име';
$_MODULE['<{statsbestsuppliers}theme958>statsbestsuppliers_2a0440eec72540c5b30d9199c01f348c'] = 'Продадено количество';
$_MODULE['<{statsbestsuppliers}theme958>statsbestsuppliers_ea067eb37801c5aab1a1c685eb97d601'] = 'Общо платено';
$_MODULE['<{statsbestsuppliers}theme958>statsbestsuppliers_cc3eb9ba7d0e236f33023a4744d0693a'] = 'Най-добри доставчици';
$_MODULE['<{statsbestsuppliers}theme958>statsbestsuppliers_37607fc64452028f4d484aa014071934'] = 'Добавя списък с най-добрите доставчици към Таблото със статистика.';
$_MODULE['<{statsbestsuppliers}theme958>statsbestsuppliers_998e4c5c80f27dec552e99dfed34889a'] = 'Експорт на CSV';
