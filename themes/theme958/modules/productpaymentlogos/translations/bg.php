<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{productpaymentlogos}theme958>productpaymentlogos_1056d03db619b016d8fc6b60d08ef488'] = 'Блок с логата на плащанията';
$_MODULE['<{productpaymentlogos}theme958>productpaymentlogos_88fd6d994459806f2dcb8999ac03c76e'] = 'Показва логата на наличните системи за плащане на страницата с продукта.';
$_MODULE['<{productpaymentlogos}theme958>productpaymentlogos_126b21ce46c39d12c24058791a236777'] = 'невалидна снимка';
$_MODULE['<{productpaymentlogos}theme958>productpaymentlogos_df7859ac16e724c9b1fba0a364503d72'] = 'Възникна грешка при опита за качване на файла.';
$_MODULE['<{productpaymentlogos}theme958>productpaymentlogos_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{productpaymentlogos}theme958>productpaymentlogos_223795d3a336ef80c5b6a070ae4e0d2a'] = 'Заглавие на блока';
$_MODULE['<{productpaymentlogos}theme958>productpaymentlogos_c7dd01cfb61ff8c984c0aff44f6540e3'] = 'Можете да изберете да добавите озаглавяване над логата.';
$_MODULE['<{productpaymentlogos}theme958>productpaymentlogos_89ca5c48bbc6b7a648a5c1996767484c'] = 'Изображение на рекламния блок';
$_MODULE['<{productpaymentlogos}theme958>productpaymentlogos_9ce38727cff004a058021a6c7351a74a'] = 'Линк към изображението';
$_MODULE['<{productpaymentlogos}theme958>productpaymentlogos_826eeee52fe142372c3a2bc195eff911'] = 'Можете или да качите Ваше изображение с формуляра по-горе, или да го свържете чрез опцията \"Изображение за линк\".';
$_MODULE['<{productpaymentlogos}theme958>productpaymentlogos_c9cc8cce247e49bae79f15173ce97354'] = 'Запазване';
