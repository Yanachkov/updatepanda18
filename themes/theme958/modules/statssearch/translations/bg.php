<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statssearch}theme958>statssearch_8c3c245232744602822902b97e65d6f9'] = 'Търси в магазина';
$_MODULE['<{statssearch}theme958>statssearch_39ea81e520d21ea8e5b8265a177c07e0'] = 'Добавя раздел към Таблото със статистика, което показва коя ключова дума е била търсена от Ваш посетител на магазина.';
$_MODULE['<{statssearch}theme958>statssearch_867343577fa1f33caa632a19543bd252'] = 'Ключови думи';
$_MODULE['<{statssearch}theme958>statssearch_e52e6aa1a43a0187e44f048f658db5f9'] = 'Повтаряемост';
$_MODULE['<{statssearch}theme958>statssearch_fd69c5cf902969e6fb71d043085ddee6'] = 'Резултати';
$_MODULE['<{statssearch}theme958>statssearch_998e4c5c80f27dec552e99dfed34889a'] = 'Експорт на CSV';
$_MODULE['<{statssearch}theme958>statssearch_710af2afb2786d5f79b2c9bd8a746b8b'] = 'Няма намерени ключови думи, които да са търсени повече от веднъж.';
$_MODULE['<{statssearch}theme958>statssearch_e15832aa200f342e8f4ab580b43a72a8'] = 'Топ 10 ключови думи';
$_MODULE['<{statssearch}theme958>statssearch_52ef9633d88a7480b3a938ff9eaa2a25'] = 'Други';
