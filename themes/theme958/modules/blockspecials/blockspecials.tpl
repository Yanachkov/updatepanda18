<!-- MODULE Block specials -->
<div id="special_block_right" class="block">
	<p class="title_block">
        <a href="{$link->getPageLink('prices-drop')|escape:'html':'UTF-8'}" title="{l s='Specials' mod='blockspecials'}">
            {l s='Specials' mod='blockspecials'}
        </a>
    </p>
	<div class="block_content products-block">
    {if $special}
		<ul>
        	<li class="clearfix">
            	<a class="products-block-image img-responsive" href="{$special.link|escape:'html':'UTF-8'}">
                    <img 
                    class="replace-2x img-responsive" 
                    src="{$link->getImageLink($special.link_rewrite, $special.id_image, 'home_default')|escape:'html':'UTF-8'}" 
                    alt="{$special.legend|escape:'html':'UTF-8'}" 
                    title="{$special.name|escape:'html':'UTF-8'}" />
                    {if isset($special.on_sale) && $special.on_sale && isset($special.show_price) && $special.show_price && !$PS_CATALOG_MODE}
                        <span class="sale-box">
                            <span class="sale-label">{l s='Sale!'}</span>
                        </span>
                    {/if}
                </a>
                <div class="product-content">
                	<div class="price-box">
                    	{if !$PS_CATALOG_MODE}
                        	<span class="price product-price special-price">
                                {if !$priceDisplay}
                                    {displayWtPrice p=$special.price}{else}{displayWtPrice p=$special.price_tax_exc}
                                {/if}
                            </span>
                             {if $special.specific_prices}
                                {assign var='specific_prices' value=$special.specific_prices}
                                {if $specific_prices.reduction_type == 'percentage' && ($specific_prices.from == $specific_prices.to OR ($smarty.now|date_format:'%Y-%m-%d %H:%M:%S' <= $specific_prices.to && $smarty.now|date_format:'%Y-%m-%d %H:%M:%S' >= $specific_prices.from))}
                                    <span class="price-percent-reduction">-{$specific_prices.reduction*100|floatval}%</span>
                                {/if}
                            {/if}
                             <span class="product-price old-price">
                                {if !$priceDisplay}
                                    {displayWtPrice p=$special.price_without_reduction}{else}{displayWtPrice p=$priceWithoutReduction_tax_excl}
                                {/if}
                            </span>
                        {/if}
                    </div>
                	<h5>
                        <a class="product-name" href="{$special.link|escape:'html':'UTF-8'}" title="{$special.name|escape:'html':'UTF-8'}">
                            {$special.name|escape:'html':'UTF-8'|truncate:45}
                        </a>
                    </h5>
                    {if isset($special.description_short) && $special.description_short}
                    	<p class="product-description">
                            {$special.description_short|strip_tags:'UTF-8'|truncate:70}
                        </p>
                    {/if}
                </div>
            </li>
		</ul>
		<div>
			<a 
            class="btn btn-default button button-small" 
            href="{$link->getPageLink('prices-drop')|escape:'html':'UTF-8'}" 
            title="{l s='All specials' mod='blockspecials'}">
                <span>{l s='All specials' mod='blockspecials'}<i class="icon-chevron-right right"></i></span>
            </a>
		</div>
    {else}
		<div>{l s='No specials at this time.' mod='blockspecials'}</div>
    {/if}
	</div>
</div>
<!-- /MODULE Block specials -->