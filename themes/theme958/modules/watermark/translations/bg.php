<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{watermark}theme958>watermark_ee20bb60493f049175fc10c35acd2272'] = 'Воден знак';
$_MODULE['<{watermark}theme958>watermark_d31c2d99614d8e16430d2d8c99c1f2b0'] = 'Изисква се подравняване по Y.';
$_MODULE['<{watermark}theme958>watermark_8fe9c39f4bf87082caabcf3650970c71'] = 'Подравняването по Y не е в разрешените граници.';
$_MODULE['<{watermark}theme958>watermark_c2cf8e33c907a4cc689881dc8fa571c2'] = 'Изисква се подравняване по X.';
$_MODULE['<{watermark}theme958>watermark_3a1f788dbe8957be92048606cf0d3fcb'] = 'Подравняването по Х не е в разрешените граници.';
$_MODULE['<{watermark}theme958>watermark_a9cac4be0fa0b815376b96f49e1435d7'] = 'Изисква се поне един формат за изображения.';
$_MODULE['<{watermark}theme958>watermark_130aab6764f25267c79cef371270eb2a'] = 'Възникна грешка при качване на водния знак: %1$s за %2$s';
$_MODULE['<{watermark}theme958>watermark_cc99ba657a4a5ecf9d2d7cb974d25596'] = 'След конфигуриране на модула, възстановете изображенията, използвайки инструмента \"Изображения\" в Предпочитания. Въпреки това, водният знак ще бъде добавян автоматично към новите изображения.';
