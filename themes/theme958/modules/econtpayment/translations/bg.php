<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{econtpayment}theme958>confirmation_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Вашата поръчка в';
$_MODULE['<{econtpayment}theme958>confirmation_75fbf512d744977d62599cc3f0ae2bb4'] = 'е завършена!';
$_MODULE['<{econtpayment}theme958>confirmation_b930c18b58ef7207a35c90a952ccd743'] = 'Вие избрахте да получите пратката си чрез Екон.';
$_MODULE['<{econtpayment}theme958>confirmation_e6dc7945b557a1cd949bea92dd58963e'] = 'Вашата поръчка ще бъде обработена до няколко часа.';
$_MODULE['<{econtpayment}theme958>confirmation_0db71da7150c27142eef9d22b843b4a9'] = 'За повече информация се свържете с наш ';
$_MODULE['<{econtpayment}theme958>confirmation_64430ad2835be8ad60c59e7d44e4b0b1'] = 'представител продажби.';
