<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocklanguages}theme958>blocklanguages_d33f69bfa67e20063a8905c923d9cf59'] = 'Блок за избор на език';
$_MODULE['<{blocklanguages}theme958>blocklanguages_5bc2cbadb5e09b5ef9b9d1724072c4f9'] = 'Добавя блок, който позволява на посетителите да избират език за съдържанието на магазина Ви.';
