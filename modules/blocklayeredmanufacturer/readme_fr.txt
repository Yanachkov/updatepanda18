
@module			blocklayeredmanufacturer
@version		1.3.1



== Details ==
----

Ce module est léger avec beaucoup de possibilités d'utilisations de filtrage sur les pages fabricants :
	* Filtrage par catégorie (affichage uniquement des catégories qui concernent les fabricants courrant)
	* Filtrage par prix
	* Filtrage par attributs (taille, couleur,..)
	* Filtrage par caractéristiques (styles, propriétés,..)
	* Calcul des prix automatique en fonctions des réductions appliquées au groupe client
	* Compatible tablette et smartphone

Chaque type de filtre peuvent être activer/désactiver et supporte des paramètres natifs prestashop comme :
 	* multi-boutique
 	* règle de monnaie et taxonomie
 	* traduction intégrée

Avantages :
 	# Facile à installer (2 cliques)
 	# Pas de réindexation des produits (directement connecté à votre base de données vous devez donc faire attention à l'utilisation du filtrage par caractéristiques et attribut dans le cas d'une grandre base de données produits qui peut-être long à se générer)
 	# Utilise votre thème personnel appliqué au module prestashop "blocklayered"



== Astuces ==
----

Si votre module ne fonctionne pas copier ou additionner ces 2 fichiers obligatoires que vous trouverez dans le dossier du module :
	# dans override/classes/ file Manufacturer.php
	# dans override/controllers/front/ file ManufacturerController.php



== Changelog ==
----

=== version 1.3.1 ===

	Date : '''2016-08-29'''
	
	Corrige l'erreur de compatibilité à l'installation de 1.5.6.0

=== version 1.3.0 ===

	Date : '''2016-08-17'''
	
	Optimisation des requêtes pour améliorer la rapidité de filtrage

=== version 1.2.1 ===

	Date : '''2016-08-13'''
	
	Mise à jour de la requête des caractéristiques parcequ'il manquait des valeurs

=== version 1.2.0 ===

	Date : '''2015-04-19'''
	
	Support de prestashop 1.5

=== version 1.1.5 ===

	Date : '''2016-04-14'''
	
	Gérer pour chaque type de filtre une limitation d'affichage (paramètre global)

=== version 1.1.4 ===

	Date : '''2016-04-09'''
	
	Corrige l'alignement de checkbox avec le label pour certaines résolutions spécifiques

=== version 1.1.3 ===

	Date : '''2016-01-16'''
	
	Find lower step categories when building first level of categories' filtering

=== version 1.1.2 ===

	Date : '''2016-01-04'''
	
	Ajout du support du filtrage par prix pour les boutiques sans règles de taxes sur leurs produits
	Recherche des sous-enfants des catégories sans enfants direct
	Nouveau paramètre pour choisir le type de bouton pour le filtrage par catégorie (bouton radio ou checkbox)
	Nouveau paramètre pour choisir le filtrage par prix (inclure prix spécifique ou pas)

=== version 1.1.1 ===

	Date : '''2015-12-18'''
	
	Ajout du support de l'attachement dans la colonne de droite

=== version 1.1.0 ===

	Date : '''2015-12-05'''
	
	Ajout du support de la recherche des caractéristiques produits

=== version 1.0.2 ===

	Date : '''2015-08-01'''
	
	Correctif d'un bug de la perte de groupe client lorsque pSql est utilisée dans une fonction et pas à l'extérieur

=== version 1.0.1 ===

	Date : '''2015-06-13'''
	
	Protection des requêtes SQL des injections par une fonction native Prestashop

=== version 1.0.0 ===

	Date : '''2015-04-29'''
	
	Première version stable



