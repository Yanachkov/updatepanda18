/**
 *
 * JS for blocklayeredmanufacturer add-on
 * @date 2015-12-21
 * @version 1.1.2
 * @author Thomas GIORDMAINA <thomas.giordmaina@42webstreet.com>
 * @copyright 42webstreet.com
 */

var changeFeat = function (id_feature_value) {
		var id_feature_values = "";
		var orig_id_feature_values = "";
		var orig_id_feature_values_arr = [];
		if(!!id_feature_value && id_feature_value > 0) {
			/**	
			 * Get previous category filtering and remove
			 */
			if($('#layered_form_man').find('input[name="ff"]').length > 0){
				orig_id_feature_values = $.trim($('#layered_form_man').find('input[name="ff"]').val());
				$('#layered_form_man').find('input[name="ff"]').remove();
			}

			orig_id_feature_values_arr = orig_id_feature_values.split(',');

			$.each(orig_id_feature_values_arr, function( index, id_fv ) {
				if(parseInt(id_fv) == id_feature_value) {
					orig_id_feature_values_arr.splice(index, 1);
				}
			});

			id_feature_values = orig_id_feature_values_arr.join(',');

			if(id_feature_values == orig_id_feature_values) {
				orig_id_feature_values_arr.push(id_feature_value);
				id_feature_values = orig_id_feature_values_arr.join(',');
			}

			if(!!id_feature_values.charAt(0) && id_feature_values.charAt(0) == ',') id_feature_values = id_feature_values.substring(1);
			$('#layered_form_man').append('<input name="ff" type="hidden" value="'+id_feature_values+'" />');			

			$('#layered_form_man').submit();
		}
};
var changeAttr = function (id_attribute) {
		var id_attributes = "";
		var orig_id_attributes = "";
		var orig_id_attributes_arr = [];
		if(!!id_attribute && id_attribute > 0) {
			/**	
			 * Get previous category filtering and remove
			 */
			if($('#layered_form_man').find('input[name="af"]').length > 0){
				orig_id_attributes = $.trim($('#layered_form_man').find('input[name="af"]').val());
				$('#layered_form_man').find('input[name="af"]').remove();
			}

			orig_id_attributes_arr = orig_id_attributes.split(',');

			$.each(orig_id_attributes_arr, function( index, id_attr ) {
				if(parseInt(id_attr) == id_attribute) {
					orig_id_attributes_arr.splice(index, 1);
				}
			});

			id_attributes = orig_id_attributes_arr.join(',');

			if(id_attributes == orig_id_attributes) {
				orig_id_attributes_arr.push(id_attribute);
				id_attributes = orig_id_attributes_arr.join(',');
			}

			if(!!id_attributes.charAt(0) && id_attributes.charAt(0) == ',') id_attributes = id_attributes.substring(1);
			$('#layered_form_man').append('<input name="af" type="hidden" value="'+id_attributes+'" />');			

			$('#layered_form_man').submit();
		}
};
var changeCat = function (id_cat) {

		var orig_id_cat = 0;
		/**	
		 * Get previous category filtering and remove
		 */
		if($('#layered_form_man').find('input[name="c"]').length > 0){
			orig_id_cat = parseInt($('#layered_form_man').find('input[name="c"]').val());
			$('#layered_form_man').find('input[name="c"]').remove();
		}

		/**
		 * Remove price filter
		 */
		if($('#layered_form_man').find('input[name="pf"]').length > 0){
			$('#layered_form_man').find('input[name="pf"]').remove();
		}

		/**
		 * Remove attribute filter
		 */
		if($('#layered_form_man').find('input[name="af"]').length > 0){
			$('#layered_form_man').find('input[name="af"]').remove();
		}

		/**
		 * Remove feature filter
		 */
		if($('#layered_form_man').find('input[name="ff"]').length > 0){
			$('#layered_form_man').find('input[name="ff"]').remove();
		}

		/**	
		 * Check for previous category filtering
		 */
		if(orig_id_cat != id_cat){
			$('#layered_form_man').append('<input name="c" type="hidden" value="'+id_cat+'" />');
		}

		
		$('#layered_form_man').submit();

};


$(document).ready(function(){

	// Category filter
	$('.layered_filter_cat').on('click','li',function(e){
		var target = $(e.target);
		if(target.prop("tagName") == "A") {
			e.stopPropagation();
			var id_cat = parseInt(target.parent().parent().find('input').val());
			changeCat(id_cat);
		}
	});
	$('.layered_filter_cat').on('click','input.checkbox, input.radio',function(){
		var id_cat = parseInt($(this).val());
		changeCat(id_cat);
	});
	$('.layered_filter_cat').find('li.selected').parents('li').addClass('selected');
	// End: Category filter

	// Attribute filter
	$('.layered_filter_attr').on('click','li',function(e){
		var target = $(e.target);
		if(target.prop("tagName") == "A") {
			e.stopPropagation();
			var id_attribute = parseInt(target.parents('li').data('id-attribute'));
			changeAttr(id_attribute);
		}
	});
	$('.layered_filter_attr').on('click','.checkbox, .color-option',function(){
		var id_attribute = parseInt($(this).parents('li').data('id-attribute'));
		changeAttr(id_attribute);
	});
	// End: Attribute filter

	// Feature filter
	$('.layered_filter_feature').on('click','li',function(e){
		var target = $(e.target);
		if(target.prop("tagName") == "A") {
			e.stopPropagation();
			var id_feature_value = parseInt(target.parents('li').data('id-feature'));
			changeFeat(id_feature_value);
		}
	});
	$('.layered_filter_feature').on('click','.checkbox, .color-option',function(){
		var id_feature_value = parseInt($(this).parents('li').data('id-feature'));
		changeFeat(id_feature_value);
	});
	// End: Feature filter



	$('#enabled_filters_manufacturer').on('click','.layered_manufacturer_cat_del',function(){
		$('#layered_form_man').find('input[name="c"]').remove();
		$('#layered_form_man').submit();
	});

	$('#enabled_filters_manufacturer').on('click','.layered_manufacturer_price_del',function(){
		$('#layered_form_man').find('input[name="pf"]').remove();
		$('#layered_form_man').submit();
	});

	$('#enabled_filters_manufacturer').on('click','.layered_id_attribute_group_del',function(){
		var id_attribute = parseInt($(this).data('filter-attribute-id'));
		if(!!id_attribute && id_attribute > 0){
			var $current_filter_attr = $('.layered_filter_attr').find('label[name="layered_id_attribute_group_'+id_attribute+'"] a');
			$current_filter_attr.click();
		}
	});

	$('#enabled_filters_manufacturer').on('click','.layered_id_feature_group_del',function(){
		var id_feature_value = parseInt($(this).data('filter-feature-id'));
		if(!!id_feature_value && id_feature_value > 0){
			var $current_filter_feature = $('.layered_filter_feature').find('label[name="layered_id_feature_group_'+id_feature_value+'"] a');
			$current_filter_feature.click();
		}
	});

});