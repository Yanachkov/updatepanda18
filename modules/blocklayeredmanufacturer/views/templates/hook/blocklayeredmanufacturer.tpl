{*
*  @date 2016-04-14
*  Blocklayeredmanufacturer template (front)
*  @author Thomas GIORDMAINA <thomas.giordmaina@42webstreet.com>
*  @copyright 42webstreet.com
*}
<!-- Module Blocklayeredmanufacturer -->
	<div id="pageleft-Blocklayeredmanufacturer" class="block">
		<p class="title_block">{l s='Catalog' mod='blocklayeredmanufacturer'}</p>
		<div class="block_content">
			<div id="layered_form">
				<div>
				{if ( $selected_category > 0 || isset($print_active_filter_price) && $print_active_filter_price == 1 ) || ( isset($print_active_filter_attribute) && $print_active_filter_attribute == 1 ) || ( isset($print_active_filter_feature) && $print_active_filter_feature == 1 )}
				<div id="enabled_filters_manufacturer"> <span class="layered_subtitle" style="float: none;"> {l s='Active filters' mod='blocklayeredmanufacturer'} : </span>
					<ul>
						{if $selected_category > 0 }
							<li> <a href="javascript:;" class="layered_manufacturer_cat_del" title="{l s='Cancel' mod='blocklayeredmanufacturer'}">x</a> {l s='Category' mod='blocklayeredmanufacturer'} : {if isset($selected_category_name)}{$selected_category_name|escape:'htmlall':'UTF-8'}{/if}</li>

						{/if}	

						{if $print_active_filter_price == 1 }
							<li> <a href="javascript:;" class="layered_manufacturer_price_del" title="{l s='Cancel' mod='blocklayeredmanufacturer'}">x</a> {l s='Price' mod='blocklayeredmanufacturer'} : {$manufacturer_filter_price_min_selected|escape:'htmlall':'UTF-8'}{$manufacturer_filter_price_currency|escape:'htmlall':'UTF-8'} - {$manufacturer_filter_price_max_selected|escape:'htmlall':'UTF-8'}{$manufacturer_filter_price_currency|escape:'htmlall':'UTF-8'}</li>

						{/if}

						{if $print_active_filter_attribute == 1 }
							{foreach from=$filter_attribute item=filter}
								{foreach from=$filter.grouplist item=attribute}
									{if in_array($attribute.id_attribute, $filter_attribute_selected)}
										<li> <a href="javascript:;" class="layered_id_attribute_group_del" title="{l s='Cancel' mod='blocklayeredmanufacturer'}" data-filter-attribute-id="{$attribute.id_attribute|escape:'htmlall':'UTF-8'}">x</a> {$filter.public_name|escape:'htmlall':'UTF-8'} : {$attribute.name|escape:'htmlall':'UTF-8'}</li>
									{/if}
								{/foreach}
							{/foreach}
						{/if}

						{if $print_active_filter_feature == 1 }
							{foreach from=$filter_feature item=filter}
								{foreach from=$filter.grouplist item=feature}
									{if in_array($feature.id_feature_value, $filter_feature_selected)}
										<li> <a href="javascript:;" class="layered_id_feature_group_del" title="{l s='Cancel' mod='blocklayeredmanufacturer'}" data-filter-feature-id="{$feature.id_feature_value|escape:'htmlall':'UTF-8'}">x</a> {$filter.name|escape:'htmlall':'UTF-8'} : {$feature.value|escape:'htmlall':'UTF-8'}</li>
									{/if}
								{/foreach}
							{/foreach}
						{/if}
					</ul>
				</div>
				{/if}
					{if isset($print_manufacturer_filter_cat) && $print_manufacturer_filter_cat > 0}
					<div class="layered_filter layered_filter_cat">
						<div class="layered_subtitle_heading">
                            <span class="layered_subtitle">{l s='Categories' mod='blocklayeredmanufacturer'}</span>
						</div>
						{if isset($manufacturer_filter_cat)}{$manufacturer_filter_cat}{/if}
					</div>
					{/if}
					{if isset($print_manufacturer_filter_price) && $print_manufacturer_filter_price > 0}
					<div class="layered_filter layered_filter_price">
						<div class="layered_subtitle_heading">
                            <span class="layered_subtitle">{l s='Price' mod='blocklayeredmanufacturer'}</span>
						</div>
						<label for="price">{l s='Range' mod='blocklayeredmanufacturer'} :</label> 
						<span id="layered_man_price_range">{$manufacturer_filter_price_min_selected|escape:'htmlall':'UTF-8'}{$manufacturer_filter_price_currency|escape:'htmlall':'UTF-8'} - {$manufacturer_filter_price_max_selected|escape:'htmlall':'UTF-8'}{$manufacturer_filter_price_currency|escape:'htmlall':'UTF-8'}</span>
						<div class="layered_slider_container">
							<div class="layered_slider" id="layered_man_price_slider"></div>
						</div>
						<script type="text/javascript">
						{literal}
							var filterRange = {/literal}{$manufacturer_filter_price_max|escape:'htmlall':'UTF-8'}-{$manufacturer_filter_price_min|escape:'htmlall':'UTF-8'}{literal};
							var step = Math.round(filterRange / 100);
							if (step > 1)
								step = parseInt(step);
							$('#layered_man_price_slider').slider({
								range: true,
								step: step,
								min: {/literal}{$manufacturer_filter_price_min|escape:'htmlall':'UTF-8'}{literal},
								max: {/literal}{$manufacturer_filter_price_max|escape:'htmlall':'UTF-8'}{literal},
								values: [{/literal}{if isset($manufacturer_filter_price_min_selected, $manufacturer_filter_price_max_selected) && $manufacturer_filter_price_min_selected > -1 && $manufacturer_filter_price_max_selected > -1 && $manufacturer_filter_price_max_selected > $manufacturer_filter_price_min_selected}{$manufacturer_filter_price_min_selected|escape:'htmlall':'UTF-8'} , {$manufacturer_filter_price_max_selected|escape:'htmlall':'UTF-8'}{/if}{literal}],
								slide: function( event, ui ) {
									if(ui.values[0] == ui.values[1]){
										$(this).slider( "values", {/literal}{if isset($manufacturer_filter_price_min_selected, $manufacturer_filter_price_max_selected) && $manufacturer_filter_price_min_selected > -1 && $manufacturer_filter_price_max_selected > -1}{$manufacturer_filter_price_min_selected|escape:'htmlall':'UTF-8'} , {$manufacturer_filter_price_max_selected|escape:'htmlall':'UTF-8'}{else}{$manufacturer_filter_price_min|escape:'htmlall':'UTF-8'}, {$manufacturer_filter_price_max|escape:'htmlall':'UTF-8'}{/if}{literal} );
										return false;
									}
									if(ui.values[1] < ui.values[0]) return false;
									if(ui.values[0] > ui.values[1]) return false;
									$('#layered_man_price_range').html(ui.values[0]+'{/literal}{$manufacturer_filter_price_currency|escape:'htmlall':'UTF-8'}{literal} - '+ui.values[1]+'{/literal}{$manufacturer_filter_price_currency|escape:'htmlall':'UTF-8'}{literal}');
								},
								stop: function ( event, ui ) {
									var current_slide_values = ui.values[0]+'_'+ui.values[1];
									/**	
									 * Get previous price filtering
									 */
									if($('#layered_form_man').find('input[name="pf"]').val() == current_slide_values) return;

									if($('#layered_form_man').find('input[name="pf"]').length > 0){
										$('#layered_form_man').find('input[name="pf"]').remove();
									}

									$('#layered_form_man').append('<input name="pf" type="hidden" value="'+current_slide_values+'" />');
									
									$('#layered_form_man').submit();
								}
							});
						{/literal}
						</script>
					</div>
					{/if}
					{if isset($filter_attribute, $print_manufacturer_filter_attribute) && $print_manufacturer_filter_attribute > 0}
						{foreach from=$filter_attribute item=filter}
							<div class="layered_filter layered_filter_attr">
								<div class="layered_subtitle_heading">
		                            <span class="layered_subtitle">{if isset($filter.public_name)}{$filter.public_name|escape:'htmlall':'UTF-8'}{/if}</span>
								</div>
								{assign var=cpt_attr value=1}
								{if $filter.is_color_group == 1}
									<ul id="ul_layered_id_attribute_group_{$filter.pos|escape:'htmlall':'UTF-8'}" class="col-lg-12 layered_filter_ul color-group">
									{foreach from=$filter.grouplist item=attribute}
										{if isset($print_manufacturer_filter_attribute_max_limit) && $print_manufacturer_filter_attribute_max_limit != 0 && $print_manufacturer_filter_attribute_max_limit < $cpt_attr}
											{break}
										{/if}
										<li class="nomargin hiddable col-lg-6{if $attribute.selected == 1} selected{/if}" data-id-attribute="{$attribute.id_attribute|escape:'htmlall':'UTF-8'}">
										<input class="color-option input-attr" type="button" name="layered_id_attribute_group_{$attribute.id_attribute|escape:'htmlall':'UTF-8'}" id="layered_id_attribute_group_{$attribute.id_attribute|escape:'htmlall':'UTF-8'}" style="background: {$attribute.color|escape:'htmlall':'UTF-8'};">
										<label for="layered_id_attribute_group_{$attribute.id_attribute|escape:'htmlall':'UTF-8'}" name="layered_id_attribute_group_{$attribute.id_attribute|escape:'htmlall':'UTF-8'}" class="layered_color label-attr" title="{$attribute.name|escape:'htmlall':'UTF-8'}">
											<a href="javascript:;">{$attribute.name|escape:'htmlall':'UTF-8'}</a>
										</label>
										</li>
									{assign var=cpt_attr value=$cpt_attr+1}
									{/foreach}
									</ul>
								{else}
									<ul id="ul_layered_id_attribute_group_{$filter.pos|escape:'htmlall':'UTF-8'}" class="col-lg-12 layered_filter_ul color-group">
									{foreach from=$filter.grouplist item=attribute}
										{if isset($print_manufacturer_filter_attribute_max_limit) && $print_manufacturer_filter_attribute_max_limit != 0 && $print_manufacturer_filter_attribute_max_limit < $cpt_attr}
											{break}
										{/if}
										<li class="nomargin hiddable col-lg-6{if $attribute.selected == 1} selected{/if}" data-id-attribute="{$attribute.id_attribute|escape:'htmlall':'UTF-8'}">
										<input type="checkbox" class="checkbox input-attr" name="layered_id_attribute_group_{$attribute.id_attribute|escape:'htmlall':'UTF-8'}" id="layered_id_attribute_group_{$attribute.id_attribute|escape:'htmlall':'UTF-8'}"{if $attribute.selected == 1} checked="checked"{/if}>
										<label for="layered_id_attribute_group_{$attribute.id_attribute|escape:'htmlall':'UTF-8'}" name="layered_id_attribute_group_{$attribute.id_attribute|escape:'htmlall':'UTF-8'}" class="label-attr" title="{$attribute.name|escape:'htmlall':'UTF-8'}">
											<a href="javascript:;">{$attribute.name|escape:'htmlall':'UTF-8'}</a>
										</label>
										</li>
									{assign var=cpt_attr value=$cpt_attr+1}
									{/foreach}
									</ul>
								{/if}
							</div>
						{/foreach}
					{/if}
					{if isset($filter_feature, $print_manufacturer_filter_feature) && $print_manufacturer_filter_feature > 0}
						{foreach from=$filter_feature item=filter}
							<div class="layered_filter layered_filter_feature">
								<div class="layered_subtitle_heading">
		                            <span class="layered_subtitle">{if isset($filter.name)}{$filter.name|escape:'htmlall':'UTF-8'}{/if}</span>
								</div>
								<ul id="ul_layered_id_feature_group_{$filter.pos|escape:'htmlall':'UTF-8'}" class="col-lg-12 layered_filter_ul color-group">
								{assign var=cpt_feat value=1}
								{foreach from=$filter.grouplist item=feature}
									{if isset($print_manufacturer_filter_feature_max_limit) && $print_manufacturer_filter_feature_max_limit != 0 && $print_manufacturer_filter_feature_max_limit < $cpt_feat}
										{break}
									{/if}
									<li class="nomargin hiddable col-lg-6{if $feature.selected == 1} selected{/if}" data-id-feature="{$feature.id_feature_value|escape:'htmlall':'UTF-8'}">
									<input type="checkbox" class="checkbox input-attr" name="layered_id_feature_group_{$feature.id_feature_value|escape:'htmlall':'UTF-8'}" id="layered_id_feature_group_{$feature.id_feature_value|escape:'htmlall':'UTF-8'}"{if $feature.selected == 1} checked="checked"{/if}>
									<label for="layered_id_feature_group_{$feature.id_feature_value|escape:'htmlall':'UTF-8'}" name="layered_id_feature_group_{$feature.id_feature_value|escape:'htmlall':'UTF-8'}" class="label-attr" title="{$feature.value|escape:'htmlall':'UTF-8'}">
										<a href="javascript:;">{$feature.value|escape:'htmlall':'UTF-8'}</a>
									</label>
									</li>
								{assign var=cpt_feat value=$cpt_feat+1}
								{/foreach}
								</ul>
							</div>
						{/foreach}
					{/if}
				</div>
			</div>
			<form id="layered_form_man" method="GET">
				{$manufacturer_filter_input}
			</form>
		</div>
	</div>
<!-- /Module Blocklayeredmanufacturer -->