<?php
/**
 * 
 * Main module page which manage filtering on manufacturer page
 * @date 2016-08-29
 * @version 1.3.1
 * @author Thomas GIORDMAINA <thomas.giordmaina@42webstreet.com>
 * @copyright 42webstreet.com
 *
 */

/**
 * @since   1.6.0
 */
 

if (!defined('_PS_VERSION_'))
	exit;

if(!function_exists('sanitize_attribute_array')) {
	function sanitize_attribute_array (&$tabItem, $key) {
		$tabItem = (int) $tabItem;
	}
}

class blocklayeredmanufacturer extends Module
{
	private $products;
	private $nbr_products;
	private $page = 1;
	private $_html = '';
	private $PricesRS;
	private $PricesRSReducted;
	private static $aManModeCat = array("checkbox", "radio");
	private static $aManOrderCat = array("alpha", "position");
	private static $aCalcPriceMethod = array("reduction_excl", "reduction_incl");

	public function __construct()
	{
		$this->name = 'blocklayeredmanufacturer';
		$this->tab = 'front_office_features';
		$this->version = '1.3.1';
		$this->author = '42webstreet';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
		$this->module_key = "c5e0aac85a440ca18089f0032039a594";
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Filtering on manufacturer page');
		$this->description = $this->l('Enable to filter products on manufacturer page');
        if (version_compare(_PS_VERSION_, '1.5.6.0', '>') === true) {
                $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        }

		if ((int)Tools::getValue('p'))
			$this->page = (int)Tools::getValue('p');

	}


	/**
	 * @see Module::install()
	 */
	public function install()
	{
		/* Adds Module */
		if (parent::install() &&
			$this->registerHook('displayHeader') &&
			$this->registerHook('displayLeftColumn') &&
			$this->registerHook('displayRightColumn')
		)
		{

			/* Sets up Global configuration */
			$res = Configuration::updateValue('BMAN', 1);
			$res &= Configuration::updateValue('BMAN_CAT', 1);
			$res &= Configuration::updateValue('BMAN_PRICE', 1);
			$res &= Configuration::updateValue('BMAN_ATTRIBUTE', 1);
			$res &= Configuration::updateValue('BMAN_FEATURE', 0);
			$res &= Configuration::updateValue('BMAN_MODE_CAT', "checkbox");
			$res &= Configuration::updateValue('BMAN_ORDER_CAT', "position");
			$res &= Configuration::updateValue('BMAN_PRICE_CALC_METHOD', "reduction_excl");
			$res &= Configuration::updateValue('BMAN_CAT_MAX_PER_LVL_LIMIT', 0);
			$res &= Configuration::updateValue('BMAN_ATTRIBUTE_MAX_LIMIT', 0);
			$res &= Configuration::updateValue('BMAN_FEATURE_MAX_LIMIT', 0);
			return (bool)$res;
		}
		return false;
	}

	/**
	 * @see Module::uninstall()
	 */
	public function uninstall()
	{
		/* Deletes Module */
		if (parent::uninstall())
		{

			/* Unsets configuration */
			$res = Configuration::deleteByName('BMAN');
			$res &= Configuration::deleteByName('BMAN_CAT');
			$res &= Configuration::deleteByName('BMAN_PRICE');
			$res &= Configuration::deleteByName('BMAN_ATTRIBUTE');
			$res &= Configuration::deleteByName('BMAN_FEATURE');
			$res &= Configuration::deleteByName('BMAN_MODE_CAT');
			$res &= Configuration::deleteByName('BMAN_ORDER_CAT');
			$res &= Configuration::deleteByName('BMAN_PRICE_CALC_METHOD');
			$res &= Configuration::deleteByName('BMAN_CAT_MAX_PER_LVL_LIMIT');
			$res &= Configuration::deleteByName('BMAN_ATTRIBUTE_MAX_LIMIT');
			$res &= Configuration::deleteByName('BMAN_FEATURE_MAX_LIMIT');

			return (bool)$res;
		}

		return false;
	}

	public function getContent()
	{
		/* Validate & process for 'display of block layered manufacturer configuration state' */
		if (Tools::isSubmit('submitBlocklayeredMan')
		)
		{
			if ($this->_postValidation())
			{
				$this->_postProcess();
			}
		}

		$this->_html .= $this->renderForm();

		return $this->_html;
	}

	private function _postValidation()
	{
		$errors = array();

		/* Validation for 'display of block layered manufacturer configuration state' */
		if (Tools::isSubmit('submitBlocklayeredMan'))
		{
			if (!Validate::isBool(Tools::getValue('BMAN')))
				$errors[] = $this->l('Invalid values');
			if (!Validate::isBool(Tools::getValue('BMAN_CAT')))
				$errors[] = $this->l('Invalid values');
			if (!Validate::isBool(Tools::getValue('BMAN_PRICE')))
				$errors[] = $this->l('Invalid values');
			if (!Validate::isBool(Tools::getValue('BMAN_ATTRIBUTE')))
				$errors[] = $this->l('Invalid values');
			if (!Validate::isBool(Tools::getValue('BMAN_FEATURE')))
				$errors[] = $this->l('Invalid values');
			if (!in_array(Tools::getValue('BMAN_MODE_CAT'),self::$aManModeCat))
				$errors[] = $this->l('Invalid values');
			if (!in_array(Tools::getValue('BMAN_ORDER_CAT'),self::$aManOrderCat))
				$errors[] = $this->l('Invalid values');
			if (!in_array(Tools::getValue('BMAN_PRICE_CALC_METHOD'),self::$aCalcPriceMethod))
				$errors[] = $this->l('Invalid values');
			if (!Validate::isInt(Tools::getValue('BMAN_CAT_MAX_PER_LVL_LIMIT')))
				$errors[] = $this->l('Invalid values');
			if (!Validate::isInt(Tools::getValue('BMAN_ATTRIBUTE_MAX_LIMIT')))
				$errors[] = $this->l('Invalid values');
			if (!Validate::isInt(Tools::getValue('BMAN_FEATURE_MAX_LIMIT')))
				$errors[] = $this->l('Invalid values');

		}

		/* Display errors if needed */
		if (count($errors))
		{
			$this->_html .= $this->displayError(implode('<br />', $errors));

			return false;
		}

		/* Returns if validation is ok */
		return true;
	}


	private function _postProcess()
	{
		$errors = array();

		/* Processes for 'display of block layered manufacturer configuration state' */
		if (Tools::isSubmit('submitBlocklayeredMan'))
		{
			$res = Configuration::updateValue('BMAN', (int)Tools::getValue('BMAN'));
			$res &= Configuration::updateValue('BMAN_CAT', (int)Tools::getValue('BMAN_CAT'));
			$res &= Configuration::updateValue('BMAN_PRICE', (int)Tools::getValue('BMAN_PRICE'));
			$res &= Configuration::updateValue('BMAN_ATTRIBUTE', (int)Tools::getValue('BMAN_ATTRIBUTE'));
			$res &= Configuration::updateValue('BMAN_FEATURE', (int)Tools::getValue('BMAN_FEATURE'));
			$res &= Configuration::updateValue('BMAN_MODE_CAT', Tools::getValue('BMAN_MODE_CAT'));
			$res &= Configuration::updateValue('BMAN_ORDER_CAT', Tools::getValue('BMAN_ORDER_CAT'));
			$res &= Configuration::updateValue('BMAN_PRICE_CALC_METHOD', Tools::getValue('BMAN_PRICE_CALC_METHOD'));
			$res &= Configuration::updateValue('BMAN_CAT_MAX_PER_LVL_LIMIT', Tools::getValue('BMAN_CAT_MAX_PER_LVL_LIMIT'));
			$res &= Configuration::updateValue('BMAN_ATTRIBUTE_MAX_LIMIT', Tools::getValue('BMAN_ATTRIBUTE_MAX_LIMIT'));
			$res &= Configuration::updateValue('BMAN_FEATURE_MAX_LIMIT', Tools::getValue('BMAN_FEATURE_MAX_LIMIT'));
			$this->clearCache();

			if (!$res)
				$errors[] = $this->displayError($this->l('The configuration could not be updated.'));
			else
				Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=6&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
		}

		/* Display errors if needed */
		if (count($errors))
			$this->_html .= $this->displayError(implode('<br />', $errors));
		elseif (Tools::isSubmit('submitBlocklayeredMan'))
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=6&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
	}


	public function renderForm()
	{
		$sSwitchMode = 'radio';
		if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
			$sSwitchMode = 'switch';
		}
		$mode_cat_type = array(
							array(
								'id' => 'checkbox',
								'name' => $this->l('checkbox')
							),
							array(
								'id' => 'radio',
								'name' => $this->l('radio')
							)
		);
		$order_cat_type = array(
							array(
								'id' => 'alpha',
								'name' => $this->l('alphabetical')
							),
							array(
								'id' => 'position',
								'name' => $this->l('position')
							)
		);
		$price_calc_method = array(
							array(
								'id' => 'reduction_excl',
								'name' => $this->l('excluding specific price')
							),
							array(
								'id' => 'reduction_incl',
								'name' => $this->l('including specific price')
							)
		);
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => $sSwitchMode,
						'label' => $this->l('Enabled filtering on manufacturer page'),
						'name' => 'BMAN',
						'class'     => 't',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => $sSwitchMode,
						'label' => $this->l('Enabled categories filtering on manufacturer page'),
						'name' => 'BMAN_CAT',
						'is_bool' => true,
						'class'     => 't',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => $sSwitchMode,
						'label' => $this->l('Enabled price filtering on manufacturer page'),
						'name' => 'BMAN_PRICE',
						'is_bool' => true,
						'class'     => 't',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => $sSwitchMode,
						'label' => $this->l('Enabled attributes filtering on manufacturer page'),
						'name' => 'BMAN_ATTRIBUTE',
						'is_bool' => true,
						'class'     => 't',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => $sSwitchMode,
						'label' => $this->l('Enabled features filtering on manufacturer page'),
						'name' => 'BMAN_FEATURE',
						'is_bool' => true,
						'class'     => 't',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => 'select',
						'label' => $this->l('Printing mode of categories'),
						'name' => 'BMAN_MODE_CAT',
	                    'options' => array(
	                        'query' => $mode_cat_type,
	                        'id' => 'id',
	                        'name' => 'name'
	                    ),
	                ),
					array(
						'type' => 'select',
						'label' => $this->l('Printing order of categories'),
						'name' => 'BMAN_ORDER_CAT',
	                    'options' => array(
	                        'query' => $order_cat_type,
	                        'id' => 'id',
	                        'name' => 'name'
	                    ),
	                ),
					array(
						'type' => 'select',
						'label' => $this->l('Price calculation method in filtering'),
						'name' => 'BMAN_PRICE_CALC_METHOD',
	                    'options' => array(
	                        'query' => $price_calc_method,
	                        'id' => 'id',
	                        'name' => 'name'
	                    ),
	                ),
					array(
						'type' => 'text',
						'label' => $this->l('Max limitation of categories by level'),
						'name' => 'BMAN_CAT_MAX_PER_LVL_LIMIT',
						'maxlength' => '3',
						'class' => 'fixed-width-xs',
	                ),
					array(
						'type' => 'text',
						'label' => $this->l('Max limitation of all attributes filters'),
						'name' => 'BMAN_ATTRIBUTE_MAX_LIMIT',
						'maxlength' => '3',
						'class' => 'fixed-width-xs',
	                ),
					array(
						'type' => 'text',
						'label' => $this->l('Max limitation of all features filters'),
						'name' => 'BMAN_FEATURE_MAX_LIMIT',
						'maxlength' => '3',
						'class' => 'fixed-width-xs',
	                ),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitBlocklayeredMan';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}


	private function _prepareHook()
	{
		$this->context = Context::getContext();
		$oManufacturer = $this->context->controller;

		if($oManufacturer->php_self != 'manufacturer')
			return;
		if(!is_object($oManufacturer->getManufacturer()))
			return;

		$this->clearCache();

		$id_manufacturer = (int) $oManufacturer->getManufacturer()->id_manufacturer;

		$CurrentConfig = $this->getConfigFieldsValues();
		if(!isset($CurrentConfig['BMAN']) || (int) $CurrentConfig['BMAN'] == 0)
			return false;
		$print_manufacturer_filter_cat = true;
		if(!isset($CurrentConfig['BMAN_CAT']) || (int) $CurrentConfig['BMAN_CAT'] == 0)
			$print_manufacturer_filter_cat = false;
		$print_manufacturer_filter_price = true;
		if(!isset($CurrentConfig['BMAN_PRICE']) || (int) $CurrentConfig['BMAN_PRICE'] == 0)
			$print_manufacturer_filter_price = false;
		$print_manufacturer_filter_attribute = true;
		if(!isset($CurrentConfig['BMAN_ATTRIBUTE']) || (int) $CurrentConfig['BMAN_ATTRIBUTE'] == 0)
			$print_manufacturer_filter_attribute = false;
		$print_manufacturer_filter_feature = true;
		if(!isset($CurrentConfig['BMAN_FEATURE']) || (int) $CurrentConfig['BMAN_FEATURE'] == 0)
			$print_manufacturer_filter_feature = false;
		$print_manufacturer_filter_attribute_max_limit = 0;
		if(isset($CurrentConfig['BMAN_ATTRIBUTE_MAX_LIMIT']) && (int) $CurrentConfig['BMAN_ATTRIBUTE_MAX_LIMIT'] > 0)
			$print_manufacturer_filter_attribute_max_limit = (int) $CurrentConfig['BMAN_ATTRIBUTE_MAX_LIMIT'];
		$print_manufacturer_filter_feature_max_limit = 0;
		if(isset($CurrentConfig['BMAN_FEATURE_MAX_LIMIT']) && (int) $CurrentConfig['BMAN_FEATURE_MAX_LIMIT'] > 0)
			$print_manufacturer_filter_feature_max_limit = (int) $CurrentConfig['BMAN_FEATURE_MAX_LIMIT'];

		/**
		 * Prepare categories filter
		 */
		$selected_category = 0;
		$selected_category_name = "";
		if(Tools::getValue('c')) {
			$selected_category = (int) Tools::getValue('c');
			$oCategory = new Category( $selected_category );
			$selected_category_name = $oCategory->name[1];
		}

		/**
		 * Prepare attribute(s) filter(s)
		 */
		$selected_attribute_filter  = "";
		$aselected_attribute_filter = array();
		$print_active_filter_attribute = 0;
		if(Tools::getValue('af')) {
			$sSelected_attribute_filter = (string) Tools::getValue('af');
			$aselected_attribute_filter = explode(",", trim($sSelected_attribute_filter));
			if(is_array($aselected_attribute_filter) && count($aselected_attribute_filter) > 0) {
				array_walk($aselected_attribute_filter, "sanitize_attribute_array");
				$selected_attribute_filter = implode(",", $aselected_attribute_filter);
				$print_active_filter_attribute = 1;
			}
		}

		/**
		 * Prepare feature(s) filter(s)
		 */
		$selected_feature_filter  = "";
		$aselected_feature_filter = array();
		$print_active_filter_feature = 0;
		if(Tools::getValue('ff')) {
			$sSelected_feature_filter = (string) Tools::getValue('ff');
			$aselected_feature_filter = explode(",", trim($sSelected_feature_filter));
			if(is_array($aselected_feature_filter) && count($aselected_feature_filter) > 0) {
				array_walk($aselected_feature_filter, "sanitize_attribute_array");
				$selected_feature_filter = implode(",", $aselected_feature_filter);
				$print_active_filter_feature = 1;
			}
		}


		/**
		 * Build categories filter
		 */
		$CategoryMenu = $this->getCategoryMenu($id_manufacturer);

		/**
		 * Prepare and Build price filter
		 */
		$currencyShop = $this->context->currency->sign;
		$idCountry = (int) $this->context->country->id;
		$context_shop_id = (int) $this->context->shop->id;
		// $minPrice = $this->getMinPrice($idCountry, $id_manufacturer, $context_shop_id, $selected_category, $selected_attribute_filter);
		// $maxPrice = $this->getMaxPrice($idCountry, $id_manufacturer, $context_shop_id, $selected_category, $selected_attribute_filter);
		$minPrice = $this->getMinPrice($idCountry, $id_manufacturer, $context_shop_id, $selected_category);
		$maxPrice = $this->getMaxPrice($idCountry, $id_manufacturer, $context_shop_id, $selected_category);
		$minSelectedPrice = $minPrice;
		$maxSelectedPrice = $maxPrice;
		$selected_price_filter = "";
		$aselected_price_filter = null;
		$print_active_filter_price = 0;
		if(Tools::getValue('pf')) {
			$sSelected_price_filter = (string) Tools::getValue('pf');
			$aPFTab = explode("_", trim($sSelected_price_filter));
			if(isset($aPFTab[0], $aPFTab[1]) && is_numeric($aPFTab[0]) && is_numeric($aPFTab[1]) && $aPFTab[1] > $aPFTab[0]) {
				$print_active_filter_price = 1;
				$minSelectedPrice = (float) $aPFTab[0];
				$maxSelectedPrice = (float) $aPFTab[1];
				$selected_price_filter = $aPFTab[0]."_".$aPFTab[1];				
				$aselected_price_filter = array("min" => $minSelectedPrice, "max" => $maxSelectedPrice);
			}
		}

		/**
		 * Build attribute(s) filter(s)
		 */
		// $aFilterAttribute = $this->getAttributeGroup($id_manufacturer, $selected_category, $aselected_price_filter, $idCountry);
		$aFilterAttribute = $this->getAttributeGroup($id_manufacturer, $selected_category);
		$afacpt = 0;
		foreach ($aFilterAttribute as &$attribute) {
			$attribute['pos'] = $afacpt++;
			// $attribute['grouplist'] = $this->getAttributeGroupList($id_manufacturer, (int) $attribute['id_attribute_group'], $selected_category, $aselected_price_filter, $idCountry);
			$attribute['grouplist'] = $this->getAttributeGroupList($id_manufacturer, (int) $attribute['id_attribute_group'], $selected_category);
			if(count($attribute['grouplist']) > 0) {
				foreach ($attribute['grouplist'] as &$grouplist) {
					if(in_array((int)$grouplist['id_attribute'], $aselected_attribute_filter)){
						$grouplist['selected'] = 1;
					} else {
						$grouplist['selected'] = 0;
					}
				}
			} else {
				unset($aFilterAttribute[$afacpt-1]);
			}
		}


		/**
		 * Build feature(s) filter(s)
		 */
		// $aFilterAttribute = $this->getAttributeGroup($id_manufacturer, $selected_category, $aselected_price_filter, $idCountry);
		$aFilterFeature = $this->getFeatureGroup($id_manufacturer, $selected_category);
		$afacpt = 0;
		foreach ($aFilterFeature as &$Feature) {
			$Feature['pos'] = $afacpt++;
			// $Feature['grouplist'] = $this->getFeatureGroupList($id_manufacturer, (int) $Feature['id_Feature_group'], $selected_category, $aselected_price_filter, $idCountry);
			if(isset($Feature['id_feature'])) {
				$Feature['grouplist'] = $this->getFeatureGroupList($id_manufacturer, (int) $Feature['id_feature'], $selected_category);
				if(count($Feature['grouplist']) > 0) {
					foreach ($Feature['grouplist'] as &$grouplist) {
						if(in_array((int)$grouplist['id_feature_value'], $aselected_feature_filter)){
							$grouplist['selected'] = 1;
						} else {
							$grouplist['selected'] = 0;
						}
					}
				} else {
					unset($aFilterFeature[$afacpt-1]);
				}
			}
		}


		/**
		 * Build left manufacturer blocks
		 */
		$manufacturer_filter_input = $this->_buildFilterInput($selected_category, $selected_price_filter, $selected_attribute_filter, $selected_feature_filter);
		$manufacturer_filter_cat = $this->_buildLeftMenuMan($CategoryMenu, $selected_category);

			$this->smarty->assign(
				array(	
						'print_manufacturer_filter_cat' => ($print_manufacturer_filter_cat? 1:0),
						'print_manufacturer_filter_price' => ($print_manufacturer_filter_price? 1:0),
						'print_manufacturer_filter_attribute' => ($print_manufacturer_filter_attribute? 1:0),
						'print_manufacturer_filter_feature' => ($print_manufacturer_filter_feature? 1:0),
						'print_manufacturer_filter_attribute_max_limit' => $print_manufacturer_filter_attribute_max_limit,
						'print_manufacturer_filter_feature_max_limit' => $print_manufacturer_filter_feature_max_limit,
						'id_manufacturer' => $id_manufacturer,
						'manufacturer_filter_input' => $manufacturer_filter_input,
						'manufacturer_filter_cat' => $manufacturer_filter_cat,
						'selected_category' => $selected_category,
						'selected_category_name' => $selected_category_name,
						'manufacturer_filter_price_min' => $minPrice,
						'manufacturer_filter_price_max' => $maxPrice,
						'manufacturer_filter_price_min_selected' => $minSelectedPrice,
						'manufacturer_filter_price_max_selected' => $maxSelectedPrice,
						'manufacturer_filter_price_currency' => $currencyShop,
						'filter_attribute' => $aFilterAttribute,
						'filter_attribute_selected' => $aselected_attribute_filter,
						'filter_feature' => $aFilterFeature,
						'filter_feature_selected' => $aselected_feature_filter,
						'print_active_filter_price' => $print_active_filter_price,
						'print_active_filter_attribute' => $print_active_filter_attribute,
						'print_active_filter_feature' => $print_active_filter_feature
				)
			);
		return true;
	}

	public function hookdisplayHeader($params)
	{
		$this->context->controller->addCSS($this->_path.'views/css/blocklayeredmanufacturer.css');
		$this->context->controller->addCSS(_PS_JS_DIR_.'jquery/ui/themes/base/jquery.ui.all.css');
		$this->context->controller->addJQueryUI('*');
		$this->context->controller->addJQueryUI('ui.slider');
		$this->context->controller->addJS($this->_path.'views/js/blocklayeredmanufacturer.js');
	}

	public function hookdisplayLeftColumn($params)
	{
		if (!$this->_prepareHook())
			return false;
		if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
			return $this->display(__FILE__, 'blocklayeredmanufacturer.tpl', $this->getCacheId());
		} else {
			return $this->display(__FILE__, 'blocklayeredmanufacturer-1.5.tpl', $this->getCacheId());			
		}
	}

	public function hookdisplayRightColumn($params)
	{
		if (!$this->_prepareHook())
			return false;
		if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
			return $this->display(__FILE__, 'blocklayeredmanufacturer.tpl', $this->getCacheId());
		} else {
			return $this->display(__FILE__, 'blocklayeredmanufacturer-1.5.tpl', $this->getCacheId());			
		}
	}

	public function getConfigFieldsValues()
	{
		return array(
			'BMAN' => Tools::getValue('BMAN', Configuration::get('BMAN')),
			'BMAN_CAT' => Tools::getValue('BMAN_CAT', Configuration::get('BMAN_CAT')),
			'BMAN_PRICE' => Tools::getValue('BMAN_PRICE', Configuration::get('BMAN_PRICE')),
			'BMAN_ATTRIBUTE' => Tools::getValue('BMAN_ATTRIBUTE', Configuration::get('BMAN_ATTRIBUTE')),
			'BMAN_FEATURE' => Tools::getValue('BMAN_FEATURE', Configuration::get('BMAN_FEATURE')),
			'BMAN_MODE_CAT' => Tools::getValue('BMAN_MODE_CAT', Configuration::get('BMAN_MODE_CAT')),
			'BMAN_ORDER_CAT' => Tools::getValue('BMAN_ORDER_CAT', Configuration::get('BMAN_ORDER_CAT')),
			'BMAN_PRICE_CALC_METHOD' => Tools::getValue('BMAN_PRICE_CALC_METHOD', Configuration::get('BMAN_PRICE_CALC_METHOD')),
			'BMAN_CAT_MAX_PER_LVL_LIMIT' => Tools::getValue('BMAN_CAT_MAX_PER_LVL_LIMIT', Configuration::get('BMAN_CAT_MAX_PER_LVL_LIMIT')),
			'BMAN_ATTRIBUTE_MAX_LIMIT' => Tools::getValue('BMAN_ATTRIBUTE_MAX_LIMIT', Configuration::get('BMAN_ATTRIBUTE_MAX_LIMIT')),
			'BMAN_FEATURE_MAX_LIMIT' => Tools::getValue('BMAN_FEATURE_MAX_LIMIT', Configuration::get('BMAN_FEATURE_MAX_LIMIT'))
		);
	}

	public function clearCache()
	{
		if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
			$this->_clearCache('blocklayeredmanufacturer.tpl');
		} else {
			$this->_clearCache('blocklayeredmanufacturer-1.5.tpl');
		}
	}

	protected function _buildFilterInput ($selected_category = 0, $selected_price_filter = "", $selected_attribute_filter = "", $selected_feature_filter = "") {
		$html = '';
		$html.= '<input name="c" type="hidden" value="'.(int)$selected_category.'" />';
		$html.= '<input name="pf" type="hidden" value="'.$selected_price_filter.'" />';
		$html.= '<input name="af" type="hidden" value="'.$selected_attribute_filter.'" />';
		$html.= '<input name="ff" type="hidden" value="'.$selected_feature_filter.'" />';
		return $html;
	}

	protected function _buildLeftMenuMan ($category_menu = false, $selected_category = 0) {
		$CurrentConfig = $this->getConfigFieldsValues();
		if(isset($CurrentConfig['BMAN_MODE_CAT']) && in_array($CurrentConfig['BMAN_MODE_CAT'],self::$aManModeCat)) $BMAN_MODE_CAT = $CurrentConfig['BMAN_MODE_CAT'];
		else $BMAN_MODE_CAT = 'checkbox';
		$final_html = "";
		if($category_menu && count($category_menu)>0){
			$level_category_menu = array();
			foreach ($category_menu as $key => $category) {
				$level_category_menu[(int)$category['level_depth']][] = array_merge($category, array("category_menu_key" => $key));
			}
			$max_per_level = 0;
			if(isset($CurrentConfig['BMAN_CAT_MAX_PER_LVL_LIMIT'])) {
				$max_per_level = (int) $CurrentConfig['BMAN_CAT_MAX_PER_LVL_LIMIT'];
			}
			$level_depth = 2;
			$cpt_in = 0;
			if(isset($level_category_menu[$level_depth])){
				
				
				
					$level_depth = $level_depth+1;
				$final_html .= '<ul data-id="ul_layered_manufacturer_'.$cpt_in.'" class="npd col-lg-12 layered_filter_ul">';
				foreach ($level_category_menu[$level_depth] as $cat) {
					if($max_per_level > 0 && $cpt_in >= $max_per_level) {
						break;
					}
					$selected_css = $this->_SelectedCat((int)$cat['id_category'], $selected_category);
					$final_html .= '
						<li class="hiddable col-lg-12 npd'.($selected_css != ''? ' '.$selected_css : '').'">
							<input type="'.$BMAN_MODE_CAT.'" class="'.$BMAN_MODE_CAT.'" name="layered_manufacturer_cat_'.(int)$cat['id_category'].'" id="layered_manufacturer_cat_'.(int)$cat['id_category'].'" value="'.(int)$cat['id_category'].'"'.($selected_css != ''? ' checked="checked"' : '').'> 
							<label class="layered_manufacturer_cat_a" for="layered_manufacturer_cat_'.(int)$cat['id_category'].'">
								<a href="javascript:;" data-rel="nofollow" title="'.$cat['name'].'">'.$cat['name'].'</a>
							</label>';
						if($this->_hasChildren((int)$cat['id_category'], $level_category_menu, $level_depth)){
							$final_html .= $this->_buildChildren((int)$cat['id_category'], $level_category_menu, $level_depth, $selected_category, $cpt_in);
						}
						else if($this->_hasSubChildren((int)$cat['id_category'], $level_category_menu, $level_depth)){
							$final_html .= $this->_buildChildren((int)$cat['id_category'], $level_category_menu, $level_depth+1, $selected_category, $cpt_in);
						}
						$cpt_in++;
					$final_html .= '</li>';
				}
				$final_html .= '</ul>';
			} 
			else if(isset($level_category_menu[$level_depth+1])){
				
				$level_depth = $level_depth+1;
				$final_html .= '<ul data-id="ul_layered_manufacturer_'.$cpt_in.'" class="npd col-lg-12 layered_filter_ul">';
				foreach ($level_category_menu[$level_depth] as $cat) {
					if($max_per_level > 0 && $cpt_in >= $max_per_level) {
						break;
					}
					$selected_css = $this->_SelectedCat((int)$cat['id_category'], $selected_category);
					$final_html .= '
						<li class="hiddable col-lg-12 npd'.($selected_css != ''? ' '.$selected_css : '').'">
							<input type="'.$BMAN_MODE_CAT.'" class="'.$BMAN_MODE_CAT.'" name="layered_manufacturer_cat_'.(int)$cat['id_category'].'" id="layered_manufacturer_cat_'.(int)$cat['id_category'].'" value="'.(int)$cat['id_category'].'"'.($selected_css != ''? ' checked="checked"' : '').'> 
							<label class="layered_manufacturer_cat_a" for="layered_manufacturer_cat_'.(int)$cat['id_category'].'">
								<a href="javascript:;" data-rel="nofollow" title="'.$cat['name'].'">'.$cat['name'].'</a>
							</label>';
						if($this->_hasChildren((int)$cat['id_category'], $level_category_menu, $level_depth)){
							$final_html .= $this->_buildChildren((int)$cat['id_category'], $level_category_menu, $level_depth, $selected_category, $cpt_in);
						}
						else if($this->_hasSubChildren((int)$cat['id_category'], $level_category_menu, $level_depth)){
							$final_html .= $this->_buildChildren((int)$cat['id_category'], $level_category_menu, $level_depth+1, $selected_category, $cpt_in);
						}
						$cpt_in++;
					$final_html .= '</li>';
				}
				$final_html .= '</ul>';
			}
		}
		return $final_html;
	}

	protected function _SelectedCat ($id_category, $current_id_category) {
		$selectedClass = 'selected';
		if($id_category == $current_id_category) return $selectedClass;
		return '';
	}

	protected function _hasChildren ($id_category, $level_category_menu, $level_depth) {
		$child_level = (int) $level_depth + 1;
		if(isset($level_category_menu[$child_level])) {
			foreach ($level_category_menu[$child_level] as $cat) {
				if(isset($cat['id_parent']) && (int)$cat['id_parent'] == $id_category){
					return true;
				}
			}
			return false;
		} else {
			return false;
		}
	}

	protected function _hasSubChildren ($id_category, $level_category_menu, $level_depth) {
		$child_level = (int) $level_depth + 2;
		if(isset($level_category_menu[$child_level])) {
			foreach ($level_category_menu[$child_level] as $cat) {
				if(isset($cat['id_parent']) && (int)$cat['id_parent'] == $id_category){
					return true;
				}
			}
			return false;
		} else {
			return false;
		}
	}

	protected function _buildChildren ($id_category, $level_category_menu, $level_depth, $selected_category, $cpt = 0) {
		$CurrentConfig = $this->getConfigFieldsValues();
		if(isset($CurrentConfig['BMAN_MODE_CAT']) && in_array($CurrentConfig['BMAN_MODE_CAT'],self::$aManModeCat)) $BMAN_MODE_CAT = $CurrentConfig['BMAN_MODE_CAT'];
		else $BMAN_MODE_CAT = 'checkbox';
		$sHtml = '';
		$max_per_level = 0;
		if(isset($CurrentConfig['BMAN_CAT_MAX_PER_LVL_LIMIT'])) {
			$max_per_level = (int) $CurrentConfig['BMAN_CAT_MAX_PER_LVL_LIMIT'];
		}
		$cpt_in = (int) $cpt;
		$cpt_local_level = 0;
		$child_level = (int) $level_depth + 1;
		if(isset($level_category_menu[$child_level])) {
			$sHtml .= '<ul data-id="ul_layered_manufacturer_child_'.$child_level."_".$cpt_in.'" class="child col-lg-12 layered_filter_ul">';
			foreach ($level_category_menu[$child_level] as $cat) {
				if(isset($cat['id_parent']) && (int)$cat['id_parent'] == $id_category){
					if($max_per_level > 0 && $cpt_local_level >= $max_per_level) {
						break;
					}
					$selected_css = $this->_SelectedCat((int)$cat['id_category'], $selected_category);
					$sHtml .= '
						<li class="hiddable col-lg-12 child'.($selected_css != ''? ' '.$selected_css : '').'">
							<input type="'.$BMAN_MODE_CAT.'" class="'.$BMAN_MODE_CAT.'" name="layered_manufacturer_cat_'.(int)$cat['id_category'].'" id="layered_manufacturer_cat_'.(int)$cat['id_category'].'" value="'.(int)$cat['id_category'].'"'.($selected_css != ''? ' checked="checked"' : '').'>
							<label class="layered_manufacturer_cat_a" for="layered_manufacturer_cat_'.(int)$cat['id_category'].'">
								<a href="javascript:;" data-rel="nofollow" title="'.$cat['name'].'">'.$cat['name'].'</a>
							</label>';
						if($this->_hasChildren((int)$cat['id_category'], $level_category_menu, $child_level)){
							$sHtml .= $this->_buildChildren((int)$cat['id_category'], $level_category_menu, $child_level, $selected_category, $cpt_in);
						}
						else if($this->_hasSubChildren((int)$cat['id_category'], $level_category_menu, $child_level)){
							$sHtml .= $this->_buildChildren((int)$cat['id_category'], $level_category_menu, $child_level+1, $selected_category, $cpt_in);
						}
					$cpt_in++;
					$cpt_local_level++;
					$sHtml .= '</li>';
				}
			}
			$sHtml .= '</ul>';
		}
		return $sHtml;
	}

	public function getGroupReduction() {
		$this->context = Context::getContext();
		$id_default_group = 0;
		$group_reduction_calc = 1;
		if( isset($this->context->customer) && is_object($this->context->customer) ) {
			$id_default_group = (int) $this->context->customer->id_default_group;
			$group = new Group($id_default_group);
			$group_reduction = (float) $group->reduction;
			if($group_reduction > 0) {
				$group_reduction_calc = (float) round((100 - $group_reduction) / 100, 2);
			}
		}
		return (float) $group_reduction_calc;
	}
	public function getCategoryMenu($id_manufacturer = false)
	{
		if(!$id_manufacturer || !is_numeric($id_manufacturer)) return false;		
		$CurrentConfig = $this->getConfigFieldsValues();
		if(isset($CurrentConfig['BMAN_ORDER_CAT']) && in_array($CurrentConfig['BMAN_ORDER_CAT'],self::$aManOrderCat)) $BMAN_ORDER_CAT = $CurrentConfig['BMAN_ORDER_CAT'];
		else $BMAN_ORDER_CAT = 'position';
		if($BMAN_ORDER_CAT == 'position') $sOrder = ' order by c.level_depth, c.position, pcl.name ASC';
		else $sOrder = ' order by c.level_depth, pcl.name ASC';
		$this->context = Context::getContext();
		$lang_id = (int)$this->context->language->id;
		$context_shop_id = (int)Shop::getContextShopID();
		$sQuery = '
			SELECT cp.id_category, c.id_parent, c.level_depth, pcl.name, pcl.link_rewrite FROM '._DB_PREFIX_.'category_product cp INNER JOIN '._DB_PREFIX_.'product p ON (p.id_product = cp.id_product AND p.id_manufacturer = '.pSQL($id_manufacturer).' AND p.active = 1) LEFT JOIN '._DB_PREFIX_.'category c ON c.id_category = cp.id_category LEFT JOIN '._DB_PREFIX_.'category_lang pcl ON pcl.id_category = cp.id_category WHERE c.active = 1 AND c.level_depth > 1 AND pcl.id_shop = '.pSQL($context_shop_id).' AND pcl.id_lang = '.pSQL($lang_id).' GROUP BY cp.id_category '.$sOrder.';
		';
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sQuery);
	}

	public function getPrices($idCountry = 0, $id_manufacturer = 0, $context_shop_id = 0, $selected_category = 0, $selected_attribute_filter = null)
	{
		if(!$idCountry || !is_numeric($idCountry) || !$id_manufacturer || !is_numeric($id_manufacturer) || !$context_shop_id || !is_numeric($context_shop_id)) return false;

		$CurrentConfig = $this->getConfigFieldsValues();
		if(!isset($CurrentConfig['BMAN_PRICE']) || (int) $CurrentConfig['BMAN_PRICE'] == 0) return false;

		if(isset($this->PricesRS) && is_array($this->PricesRS) && !empty($this->PricesRS)) {
			return $this->PricesRS;
		} else {
			$group_reduction_calc = $this->getGroupReduction();
			$sQuery = '
			SELECT ROUND(IFNULL((MIN(p.price+((p.price*t.rate)/100))),MIN(p.price)) * '. pSQL($group_reduction_calc) .',2) as price_min, ROUND(IFNULL((MAX(p.price+((p.price*t.rate)/100))),MAX(p.price)) * '. pSQL($group_reduction_calc) .',2) as price_max FROM '._DB_PREFIX_.'product p LEFT OUTER JOIN '._DB_PREFIX_.'tax_rule tr ON (tr.id_tax_rules_group = p.id_tax_rules_group AND tr.id_country = '.pSQL($idCountry).') '.
			' LEFT OUTER JOIN '._DB_PREFIX_.'tax t ON (t.id_tax = tr.id_tax AND t.active = 1) '.
			' LEFT JOIN '._DB_PREFIX_.'product_shop ps ON ps.id_product = p.id_product '.
			($selected_category > 0 ? ' LEFT JOIN '._DB_PREFIX_.'category_product cp ON cp.id_product = p.id_product' : '').
			' WHERE p.id_manufacturer = '.pSQL($id_manufacturer).
			' AND p.active = 1'.
			' AND ps.id_shop = '.pSQL($context_shop_id).
			($selected_category > 0 ? ' AND cp.id_category = '.pSQL($selected_category) : '').
			';';
			$this->PricesRS = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sQuery);
			return $this->PricesRS;
		}
	}

	public function getPricesReducted($idCountry = 0, $id_manufacturer = 0, $context_shop_id = 0, $selected_category = 0, $selected_attribute_filter = null)
	{
		if(!$idCountry || !is_numeric($idCountry) || !$id_manufacturer || !is_numeric($id_manufacturer) || !$context_shop_id || !is_numeric($context_shop_id)) return false;

		$CurrentConfig = $this->getConfigFieldsValues();
		if(!isset($CurrentConfig['BMAN_PRICE']) || (int) $CurrentConfig['BMAN_PRICE'] == 0) return false;

		if(isset($this->PricesRSReducted) && is_array($this->PricesRSReducted) && !empty($this->PricesRSReducted)) {
			return $this->PricesRSReducted;
		} else {
			$group_reduction_calc = $this->getGroupReduction();
			$this->context = Context::getContext();
			$id_shop_group = (int)Shop::getContextShopGroupID(true);
			$context_shop_id = (int)Shop::getContextShopID();
			$currency_id = (int)$this->context->currency->id;
			$id_default_group = 0;
			$id_default_customer = 0;
			if( isset($this->context->customer) && is_object($this->context->customer) ) {
				$id_default_customer = (int) $this->context->customer->id;
				$id_default_group = (int) $this->context->customer->id_default_group;
			}
			$sQuerySel = 'SELECT 
    ROUND(MIN(IF(psp.price > - 1,
        IF(psp.reduction_type = \'amount\',
			(IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') - IF(psp.reduction_tax = 1 , psp.reduction * '. pSQL($group_reduction_calc) . ' , ( psp.reduction + IFNULL(((psp.reduction * t.rate) / 100),0) ) * '. pSQL($group_reduction_calc) . '),
			(IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') - ((IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') * psp.reduction)
			),
        IF(psp.reduction_type = \'amount\',
			(IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') - IF(psp.reduction_tax = 1 , psp.reduction * '. pSQL($group_reduction_calc) . ' , ( psp.reduction + IFNULL(((psp.reduction * t.rate) / 100),0) ) * '. pSQL($group_reduction_calc) . '),
			(IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') - ((IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') * psp.reduction)
			)
	)),2)
	AS price_specific_min,
    ROUND(MAX(IF(psp.price > - 1,
        IF(psp.reduction_type = \'amount\',
			(IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') - IF(psp.reduction_tax = 1 , psp.reduction * '. pSQL($group_reduction_calc) . ' , ( psp.reduction + IFNULL(((psp.reduction * t.rate) / 100),0) ) * '. pSQL($group_reduction_calc) . '),
			(IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') - ((IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') * psp.reduction)
			),
        IF(psp.reduction_type = \'amount\',
			(IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') - IF(psp.reduction_tax = 1 , psp.reduction * '. pSQL($group_reduction_calc) . ' , ( psp.reduction + IFNULL(((psp.reduction * t.rate) / 100),0) ) * '. pSQL($group_reduction_calc) . '),
			(IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') - ((IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') * psp.reduction)
			)
	)),2)
	AS price_specific_max
FROM
    '._DB_PREFIX_.'specific_price psp
        INNER JOIN
    '._DB_PREFIX_.'product p ON p.id_product = psp.id_product
        LEFT JOIN
    '._DB_PREFIX_.'product_shop ps ON ps.id_product = psp.id_product
        LEFT OUTER JOIN
    '._DB_PREFIX_.'tax_rule tr ON (tr.id_tax_rules_group = p.id_tax_rules_group
        AND tr.id_country = '. pSQL($idCountry) .')
        LEFT OUTER JOIN
    '._DB_PREFIX_.'tax t ON (t.id_tax = tr.id_tax AND t.active = 1) '.
			($selected_category > 0 ? ' LEFT JOIN '._DB_PREFIX_.'category_product cp ON cp.id_product = p.id_product' : '').
			' WHERE p.id_manufacturer = '.pSQL($id_manufacturer).
			' AND ( psp.id_shop = 0 OR psp.id_shop = '. pSQL($context_shop_id) .' )'.
			' AND ( psp.id_shop_group = 0 OR psp.id_shop_group = '. pSQL($id_shop_group) .' )'.
			' AND ( psp.id_country = 0 OR psp.id_country = '. pSQL($idCountry) .' )'.
			' AND ( psp.id_currency = 0 OR psp.id_currency = '. pSQL($currency_id) .' )'.
			' AND ( psp.id_group = 0 OR psp.id_group = '. pSQL($id_default_group) .' )'.
			' AND ( psp.id_customer = 0 OR psp.id_customer = '. pSQL($id_default_customer) .' )'.
			' AND p.active = 1'.
			' AND ps.id_shop = '.pSQL($context_shop_id).
			($selected_category > 0 ? ' AND cp.id_category = '.pSQL($selected_category) : '').
			';';
			$this->PricesRSReducted = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sQuerySel);
			return $this->PricesRSReducted;
		}
	}

	public function getMinPrice($idCountry = 0, $id_manufacturer = 0, $context_shop_id = 0, $selected_category = 0, $selected_attribute_filter = null)
	{
		if(!$idCountry || !is_numeric($idCountry) || !$id_manufacturer || !is_numeric($id_manufacturer) || !$context_shop_id || !is_numeric($context_shop_id)) return false;
		$rPrices = $this->getPrices($idCountry, $id_manufacturer, $context_shop_id, $selected_category, $selected_attribute_filter);
		if(isset($rPrices[0]['price_min']) && (float) $rPrices[0]['price_min'] >= 0){
			$CurrentConfig = $this->getConfigFieldsValues();
			if(isset($CurrentConfig['BMAN_PRICE_CALC_METHOD']) && in_array($CurrentConfig['BMAN_PRICE_CALC_METHOD'],self::$aCalcPriceMethod)) $BMAN_PRICE_CALC_METHOD = $CurrentConfig['BMAN_PRICE_CALC_METHOD'];
			else $BMAN_PRICE_CALC_METHOD = 'reduction_excl';
			if($BMAN_PRICE_CALC_METHOD == 'reduction_excl') {
				return (int) floor($rPrices[0]['price_min']);
			} else {
				// Get reducted price Min and compare with not reducted min
				$rPricesReducted = $this->getPricesReducted($idCountry, $id_manufacturer, $context_shop_id, $selected_category, $selected_attribute_filter);
				if(isset($rPricesReducted[0]['price_specific_min']) && (float) $rPricesReducted[0]['price_specific_min'] >= 0 && (float) $rPricesReducted[0]['price_specific_min'] < (float) $rPrices[0]['price_min']){
					return (int) floor($rPricesReducted[0]['price_specific_min']);
				} else {
					return (int) floor($rPrices[0]['price_min']);
				}
			}
		} else {
			return 0;
		}
	}

	public function getMinPriceReducted($idCountry = 0, $id_manufacturer = 0, $context_shop_id = 0, $selected_category = 0, $selected_attribute_filter = null)
	{
		if(!$idCountry || !is_numeric($idCountry) || !$id_manufacturer || !is_numeric($id_manufacturer) || !$context_shop_id || !is_numeric($context_shop_id)) return false;
		$rPrices = $this->getPricesReducted($idCountry, $id_manufacturer, $context_shop_id, $selected_category, $selected_attribute_filter);
		if(isset($rPrices[0]['price_specific_min']) && (float) $rPrices[0]['price_specific_min'] >= 0){
			return (int) floor($rPrices[0]['price_specific_min']);
		} else {
			return 0;
		}
	}

	public function getMaxPrice($idCountry = 0, $id_manufacturer = 0, $context_shop_id = 0, $selected_category = 0, $selected_attribute_filter = null)
	{
		if(!$idCountry || !is_numeric($idCountry) || !$id_manufacturer || !is_numeric($id_manufacturer) || !$context_shop_id || !is_numeric($context_shop_id)) return false;
		$rPrices = $this->getPrices($idCountry, $id_manufacturer, $context_shop_id, $selected_category, $selected_attribute_filter);
		if(isset($rPrices[0]['price_max']) && (float) $rPrices[0]['price_max'] >= 0){
			$CurrentConfig = $this->getConfigFieldsValues();
			if(isset($CurrentConfig['BMAN_PRICE_CALC_METHOD']) && in_array($CurrentConfig['BMAN_PRICE_CALC_METHOD'],self::$aCalcPriceMethod)) $BMAN_PRICE_CALC_METHOD = $CurrentConfig['BMAN_PRICE_CALC_METHOD'];
			else $BMAN_PRICE_CALC_METHOD = 'reduction_excl';
			if($BMAN_PRICE_CALC_METHOD == 'reduction_excl') {
				return (int) ceil($rPrices[0]['price_max']);
			} else {
				// Get reducted price Min and compare with not reducted min
				$rPricesReducted = $this->getPricesReducted($idCountry, $id_manufacturer, $context_shop_id, $selected_category, $selected_attribute_filter);
				if(isset($rPricesReducted[0]['price_specific_max']) && (float) $rPricesReducted[0]['price_specific_max'] >= 0 && (float) $rPricesReducted[0]['price_specific_max'] > (float) $rPrices[0]['price_max']){
					return (int) ceil($rPricesReducted[0]['price_specific_max']);
				} else {
					return (int) ceil($rPrices[0]['price_max']);
				}
			}
		} else {
			return 0;
		}
	}

	public function getMaxPriceReducted($idCountry = 0, $id_manufacturer = 0, $context_shop_id = 0, $selected_category = 0, $selected_attribute_filter = null)
	{
		if(!$idCountry || !is_numeric($idCountry) || !$id_manufacturer || !is_numeric($id_manufacturer) || !$context_shop_id || !is_numeric($context_shop_id)) return false;
		$rPrices = $this->getPricesReducted($idCountry, $id_manufacturer, $context_shop_id, $selected_category, $selected_attribute_filter);
		if(isset($rPrices[0]['price_specific_max']) && (float) $rPrices[0]['price_specific_max'] >= 0){
			return (int) ceil($rPrices[0]['price_specific_max']);
		} else {
			return 0;
		}
	}

	public function getAttributeGroup($id_manufacturer = false, $selected_category = 0, $price_range = null, $idCountry = 0)
	{
		if(!$id_manufacturer || !is_numeric($id_manufacturer)) return false;
		$CurrentConfig = $this->getConfigFieldsValues();
		if(!isset($CurrentConfig['BMAN_ATTRIBUTE']) || (int) $CurrentConfig['BMAN_ATTRIBUTE'] == 0) return array();
		$this->context = Context::getContext();
		$lang_id = (int)$this->context->language->id;
		$context_shop_id = (int)Shop::getContextShopID();
		$group_reduction_calc = $this->getGroupReduction();
		$sQuery = '
			SELECT 
			    pa.id_attribute_group, agl.public_name, pag.is_color_group
			FROM
			    '._DB_PREFIX_.'product p
			INNER JOIN '._DB_PREFIX_.'product_shop ps ON (ps.id_product = p.id_product AND ps.id_shop = '.pSQL($context_shop_id).')
			INNER JOIN '._DB_PREFIX_.'product_attribute pat ON (pat.id_product = p.id_product)
			INNER JOIN '._DB_PREFIX_.'product_attribute_combination pac ON (pac.id_product_attribute = pat.id_product_attribute)
			INNER JOIN '._DB_PREFIX_.'attribute pa ON (pa.id_attribute = pac.id_attribute)
			INNER JOIN '._DB_PREFIX_.'attribute_group pag ON (pag.id_attribute_group = pa.id_attribute_group)
			INNER JOIN '._DB_PREFIX_.'attribute_group_lang agl ON (agl.id_attribute_group = pa.id_attribute_group AND agl.id_lang = '.pSQL($lang_id).')
			'.
			($selected_category > 0 ? ' INNER JOIN '._DB_PREFIX_.'category_product cp ON (cp.id_product = p.id_product AND cp.id_category = '.pSQL($selected_category).')' : '').
			' WHERE p.id_manufacturer = '.pSQL($id_manufacturer).' AND p.active = 1
			  GROUP BY pa.id_attribute_group
			ORDER BY pa.position ASC;
		';
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sQuery);
	}

	public function getAttributeGroupList($id_manufacturer = false, $id_attribute_group = false,  $selected_category = 0, $price_range = null, $idCountry = 0)
	{
		if(!$id_manufacturer || !is_numeric($id_manufacturer) || !$id_attribute_group || !is_numeric($id_attribute_group)) return false;
		$CurrentConfig = $this->getConfigFieldsValues();
		if(!isset($CurrentConfig['BMAN_ATTRIBUTE']) || (int) $CurrentConfig['BMAN_ATTRIBUTE'] == 0) return false;
		$this->context = Context::getContext();
		$lang_id = (int)$this->context->language->id;
		$context_shop_id = (int)Shop::getContextShopID();
		$group_reduction_calc = $this->getGroupReduction();
		$sQuery = '
			SELECT 
			    pa.id_attribute, al.name, pa.color 
			FROM
			    '._DB_PREFIX_.'product p
			INNER JOIN '._DB_PREFIX_.'product_shop ps ON (ps.id_product = p.id_product AND ps.id_shop = '.pSQL($context_shop_id).')
			INNER JOIN '._DB_PREFIX_.'product_attribute pat ON (pat.id_product = p.id_product)
			INNER JOIN '._DB_PREFIX_.'product_attribute_combination pac ON (pac.id_product_attribute = pat.id_product_attribute)
			INNER JOIN '._DB_PREFIX_.'attribute pa ON (pa.id_attribute = pac.id_attribute AND pa.id_attribute_group = '.pSQL($id_attribute_group).')
			INNER JOIN '._DB_PREFIX_.'attribute_lang al ON (al.id_attribute = pa.id_attribute AND al.id_lang = '.pSQL($lang_id).')
			'.
			($selected_category > 0 ? ' INNER JOIN '._DB_PREFIX_.'category_product cp ON (cp.id_product = p.id_product AND cp.id_category = '.pSQL($selected_category).')' : '').
			' WHERE p.id_manufacturer = '.pSQL($id_manufacturer).' AND p.active = 1 
			  GROUP BY pac.id_attribute
			ORDER BY pa.position ASC;
		';
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sQuery);
	}

	public function getFeatureGroup($id_manufacturer = false, $selected_category = 0, $price_range = null, $idCountry = 0)
	{
		if(!$id_manufacturer || !is_numeric($id_manufacturer)) return false;
		$CurrentConfig = $this->getConfigFieldsValues();
		if(!isset($CurrentConfig['BMAN_FEATURE']) || (int) $CurrentConfig['BMAN_FEATURE'] == 0) return array();
		$this->context = Context::getContext();
		$lang_id = (int)$this->context->language->id;
		$context_shop_id = (int)Shop::getContextShopID();
		$group_reduction_calc = $this->getGroupReduction();
		$sQuery = '
			SELECT 
			    f.id_feature, fl.name
			FROM
			    '._DB_PREFIX_.'product p
	        INNER JOIN '._DB_PREFIX_.'product_shop ps ON (ps.id_product = p.id_product AND ps.id_shop = '.pSQL($context_shop_id).')
	        INNER JOIN '._DB_PREFIX_.'feature_product fp ON (fp.id_product = p.id_product)
	        INNER JOIN '._DB_PREFIX_.'feature f ON (f.id_feature = fp.id_feature)
	        INNER JOIN '._DB_PREFIX_.'feature_lang fl ON (fl.id_feature = f.id_feature AND fl.id_lang = '.pSQL($lang_id).')'.
			($selected_category > 0 ? ' INNER JOIN '._DB_PREFIX_.'category_product cp ON (cp.id_product = p.id_product AND cp.id_category = '.pSQL($selected_category).')' : '').
		' 	WHERE
		    p.id_manufacturer = '.pSQL($id_manufacturer).' AND p.active = 1
			GROUP BY fp.id_feature
			ORDER BY f.position ASC;
		';
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sQuery);
	}

	public function getFeatureGroupList($id_manufacturer = false, $id_feature = false,  $selected_category = 0, $price_range = null, $idCountry = 0)
	{
		if(!$id_manufacturer || !is_numeric($id_manufacturer) || !$id_feature || !is_numeric($id_feature)) return false;
		$CurrentConfig = $this->getConfigFieldsValues();
		if(!isset($CurrentConfig['BMAN_FEATURE']) || (int) $CurrentConfig['BMAN_FEATURE'] == 0) return false;
		$this->context = Context::getContext();
		$lang_id = (int)$this->context->language->id;
		$context_shop_id = (int)Shop::getContextShopID();
		$group_reduction_calc = $this->getGroupReduction();
		$sQuery = '
			SELECT 
			    fv.id_feature_value, fvl.value
			FROM
			    '._DB_PREFIX_.'product p
	        INNER JOIN '._DB_PREFIX_.'product_shop ps ON (ps.id_product = p.id_product AND ps.id_shop = '.pSQL($context_shop_id).')
	        INNER JOIN '._DB_PREFIX_.'feature_product fp ON (fp.id_product = p.id_product AND fp.id_feature = '.pSQL($id_feature).')
	        INNER JOIN '._DB_PREFIX_.'feature_value fv ON (fv.id_feature_value = fp.id_feature_value)
	        INNER JOIN '._DB_PREFIX_.'feature f ON (f.id_feature = fp.id_feature)
	        INNER JOIN '._DB_PREFIX_.'feature_value_lang fvl ON (fvl.id_feature_value = fv.id_feature_value AND fvl.id_lang = '.pSQL($lang_id).')'.
			($selected_category > 0 ? ' INNER JOIN '._DB_PREFIX_.'category_product cp ON (cp.id_product = p.id_product AND cp.id_category = '.pSQL($selected_category).')' : '').'
			WHERE
			    p.id_manufacturer = '.pSQL($id_manufacturer).' AND p.active = 1
			GROUP BY fp.id_feature_value
			ORDER BY fvl.value ASC; 
		';
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sQuery);
	}
}
