
@module		 	blocklayeredmanufacturer
@version		1.3.1
@author 		Thomas GIORDMAINA <thomas.giordmaina@42webstreet.com>



== Details ==
----

This module is a lite module with lots of features and usage possibilities :
	* Categories filtering (print only categories menu for current manufacturer page)
	* Price Range filtering
	* Attribute filtering (size, color,..)
	* Feature filtering (styles, properties,..)
	* Automatic price calculation in accordance to group of customers reduction
	* Tablets and phones supported

Each type of filter can be enabled/disabled and support prestashop native parameters like :
 	* multi store
 	* currency and tax association
 	* built-in translating

Advantages :
 	# Easy to install (2 clics)
 	# Don't need to re-index products (directly plugged to your database so you may use carefully use the feature and attribute filtering if you have a huge database of products which could be long to be generated)
 	# Use your personnal theme design applied to "blocklayered" prestashop module



== Tips ==
----

If module doesn't work copy or merge these 2 mandatory files that you can found in the module directory :
	# in override/classes/ file Manufacturer.php
	# in override/controllers/front/ file ManufacturerController.php



== Changelog ==
----

=== version 1.3.1 ===

	Date : '''2016-08-29'''
	
	Fix 1.5.6.0 compliancy error on install

=== version 1.3.0 ===

	Date : '''2016-08-17'''
	
	Optimizing queries to improve speed of filtering

=== version 1.2.1 ===

	Date : '''2016-08-13'''
	
	Update features query because it missing some values

=== version 1.2.0 ===

	Date : '''2015-04-19'''
	
	Handle prestashop 1.5

=== version 1.1.5 ===

	Date : '''2016-04-14'''
	
	Handle the setup for each type of filter a limitation of printing (global setting)

=== version 1.1.4 ===

	Date : '''2016-04-09'''
	
	Fix checbox aligment with label of filtering for specific resolution

=== version 1.1.3 ===

	Date : '''2016-01-16'''
	
	Find lower step categories when building first level of categories' filtering

=== version 1.1.2 ===

	Date : '''2016-01-04'''
	
	Add support of price filtering for store without tax rules on their products
	Search of categories' sub-childrens without direct childrens
	New parameter to choose type of input for categories filtering (radio or checkbox)
	New parameter to choose type of price filtering (including specific price or not)

=== version 1.1.1 ===

	Date : '''2015-12-18'''
	
	Add support of right column attachment

=== version 1.1.0 ===

	Date : '''2015-12-05'''
	
	Add support of features' products filtering

=== version 1.0.2 ===

	Date : '''2015-08-01'''
	
	Fix a bug with losing customers group because pSql used in a function and not outer

=== version 1.0.1 ===

	Date : '''2015-06-13'''
	
	Protect SQL queries of Injections by native Prestashop function

=== version 1.0.0 ===

	Date : '''2015-04-29'''
	
	First stable release



