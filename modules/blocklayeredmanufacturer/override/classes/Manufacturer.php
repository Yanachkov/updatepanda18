<?php
/**
* 
* Manufacturer class overrider
* @date 2016-08-17
* @version 1.3.0
* @author Thomas GIORDMAINA <thomas.giordmaina@42webstreet.com>
* @copyright 42webstreet.com
*
* Fix a bug with losing customers group because pSql used in a function and not outer
* @date 2015-08-01
* @author Thomas GIORDMAINA <thomas.giordmaina@42webstreet.com>
*
*/

class Manufacturer extends ManufacturerCore
{
	public static function getProducts($id_manufacturer, $id_lang, $p, $n, $order_by = null, $order_way = null,
		$get_total = false, $active = true, $active_category = true, Context $context = null, $current_category = null, $price_range = null, $attribute_filter = null, $feature_filter = null, $group_reduction_calc = 1)
	{
		if (!$context)
			$context = Context::getContext();

		$idCountry = (int) $context->country->id;

		$front = true;
		if (!in_array($context->controller->controller_type, array('front', 'modulefront')))
			$front = false;

		if ($p < 1)
			$p = 1;

	 	if (empty($order_by) || $order_by == 'position')
	 		$order_by = 'name';

	 	if (empty($order_way)) $order_way = 'ASC';

		if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way))
			die (Tools::displayError());

		$groups = FrontController::getCurrentCustomerGroups();
		$sql_groups = count($groups) ? pSQL('IN ('.implode(',', $groups).')') : '= 1';

		$iTotal = 0;
		
		/* Get blocklayeredmanufacturer module price calculation configuration */
		$bUseSpecificPrice = false;
		$DISPLAY_BLOCKLAYERED_MAN_PRICE_CALC_METHOD = Configuration::get('BMAN_PRICE_CALC_METHOD');
		if($DISPLAY_BLOCKLAYERED_MAN_PRICE_CALC_METHOD && $DISPLAY_BLOCKLAYERED_MAN_PRICE_CALC_METHOD == 'reduction_incl') {
			$bUseSpecificPrice = true;
		}
		$sQueryPrice = "";
		$sQueryReductedPrice = "";
		if($price_range && is_array($price_range) && isset($price_range['min'], $price_range['max'])){
			$sSubQueryTaxReduction = 'psp.reduction * '. pSQL($group_reduction_calc);
			if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
				$sSubQueryTaxReduction = 'IF(psp.reduction_tax = 1 , psp.reduction * '. pSQL($group_reduction_calc) . ' , ( psp.reduction + IFNULL(((psp.reduction * t.rate) / 100),0) ) * '. pSQL($group_reduction_calc) . ')';
			}

			$sQueryPrice = ' AND ( ROUND(IFNULL((p.price+((p.price*t.rate)/100)),p.price) * '. pSQL($group_reduction_calc) .',2) >=  '.pSQL($price_range['min']).' AND ROUND(IFNULL((p.price+((p.price*t.rate)/100)),p.price) * '. pSQL($group_reduction_calc) .',2) <= '.pSQL($price_range['max']).') ';
			$sQueryReductedPrice = ' AND ROUND(IF(psp.price > - 1,
        IF(psp.reduction_type = \'amount\',
			(IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') - ' . pSQL($sSubQueryTaxReduction) .',
			(IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') - ((IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') * psp.reduction)
			),
        IF(psp.reduction_type = \'amount\',
			(IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') - ' . pSQL($sSubQueryTaxReduction) .',
			(IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') - ((IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') * psp.reduction)
			)
	),2) >=  '.pSQL($price_range['min']).' AND ROUND(IF(psp.price > - 1,
        IF(psp.reduction_type = \'amount\',
			(IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') - ' . pSQL($sSubQueryTaxReduction) .',
			(IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') - ((IFNULL((psp.price + ((psp.price * t.rate) / 100)),
                psp.price) * '. pSQL($group_reduction_calc) . ') * psp.reduction)
			),
        IF(psp.reduction_type = \'amount\',
			(IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') - ' . pSQL($sSubQueryTaxReduction) .',
			(IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') - ((IFNULL((p.price + ((p.price * t.rate) / 100)),
                p.price) * '. pSQL($group_reduction_calc) . ') * psp.reduction)
			)
	),2) <=  '.pSQL($price_range['max']).' ';
		}

		if (strpos($order_by, '.') > 0)
		{
			$order_by = explode('.', $order_by);
			$order_by = pSQL($order_by[0]).'.`'.pSQL($order_by[1]).'`';
		}
		$alias = '';
		if ($order_by == 'price')
			$alias = 'product_shop.';
		elseif ($order_by == 'name')
			$alias = 'pl.';
		elseif ($order_by == 'manufacturer_name')
		{
			$order_by = 'name';
			$alias = 'm.';
		}
		elseif ($order_by == 'quantity')
			$alias = 'stock.';
		else
			$alias = 'p.';

		$sql = '(SELECT SQL_CALC_FOUND_ROWS p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity'
			.(Combination::isFeatureActive() ? ', MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity' : '')
			.', MAX(product_attribute_shop.`id_product_attribute`) id_product_attribute
			, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`,
			pl.`meta_title`, pl.`name`, pl.`available_now`, pl.`available_later`, MAX(image_shop.`id_image`) id_image, il.`legend`, m.`name` AS manufacturer_name,
				DATEDIFF(
					product_shop.`date_add`,
					DATE_SUB(
						NOW(),
						INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
					)
				) > 0 AS new'.(Combination::isFeatureActive() ? ',MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity' : '')
			.' FROM `'._DB_PREFIX_.'product` p
			'.Shop::addSqlAssociation('product', 'p').
			(Combination::isFeatureActive() ?
			'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (p.`id_product` = pa.`id_product`)
			'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1') : '').'
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
				ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
			LEFT JOIN `'._DB_PREFIX_.'image` i
				ON (i.`id_product` = p.`id_product`)'.
			Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il
				ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
				ON (m.`id_manufacturer` = p.`id_manufacturer`)
			'.Product::sqlStock('p', 0).
			($price_range && is_array($price_range) && isset($price_range['min'], $price_range['max']) ? ' LEFT OUTER JOIN '._DB_PREFIX_.'tax_rule tr ON (tr.id_tax_rules_group = p.id_tax_rules_group AND tr.id_country = '.pSQL($idCountry).') LEFT OUTER JOIN '._DB_PREFIX_.'tax t ON (t.id_tax = tr.id_tax AND t.active = 1)' : '').
			($price_range && $bUseSpecificPrice ? ' LEFT OUTER JOIN '._DB_PREFIX_.'specific_price psp ON psp.id_product = p.id_product ' : '');

			if (Group::isFeatureActive() || $active_category || $current_category)
			{
				$sql .= 'JOIN `'._DB_PREFIX_.'category_product` cp ON (p.id_product = cp.id_product)';
				if (Group::isFeatureActive())
					$sql .= 'JOIN `'._DB_PREFIX_.'category_group` cg ON (cp.`id_category` = cg.`id_category` AND cg.`id_group` '.$sql_groups.')';
				if ($active_category)
					$sql .= 'JOIN `'._DB_PREFIX_.'category` ca ON cp.`id_category` = ca.`id_category` AND ca.`active` = 1';
				if ($current_category)
					$sql.= ' INNER JOIN `'._DB_PREFIX_.'category_product` cap ON (cap.`id_product` = p.`id_product` AND cap.`id_category` = '.pSQL((int) $current_category).')';
			}

		$sql .= 
				($attribute_filter && trim($attribute_filter) != '' ?  ' INNER JOIN '._DB_PREFIX_.'product_attribute_combination pac ON pac.id_attribute IN('.pSQL($attribute_filter).')
				INNER JOIN '._DB_PREFIX_.'product_attribute ppa ON (ppa.id_product = p.id_product AND ppa.id_product_attribute = pac.id_product_attribute)' : '' ).
				($feature_filter && trim($feature_filter) != '' ? ' INNER JOIN '._DB_PREFIX_.'feature_product pfp ON (pfp.id_product = p.id_product AND pfp.id_feature_value IN('.pSQL($feature_filter).'))' : '').'
				WHERE p.`id_manufacturer` = '.(int)$id_manufacturer.'
				'.($active ? ' AND product_shop.`active` = 1' : '').'
				'.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
				$sQueryPrice.
				($price_range && $bUseSpecificPrice ? ' AND psp.id_specific_price IS NULL ' : '').'
				GROUP BY product_shop.id_product
				ORDER BY '.$alias.'`'.bqSQL($order_by).'` '.pSQL($order_way).'
				LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n.') ';

		if($price_range && $bUseSpecificPrice) {

			$sql_specific = ' (SELECT SQL_CALC_FOUND_ROWS p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity'
			.(Combination::isFeatureActive() ? ', MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity' : '')
			.', MAX(product_attribute_shop.`id_product_attribute`) id_product_attribute
			, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`,
			pl.`meta_title`, pl.`name`, pl.`available_now`, pl.`available_later`, MAX(image_shop.`id_image`) id_image, il.`legend`, m.`name` AS manufacturer_name,
				DATEDIFF(
					product_shop.`date_add`,
					DATE_SUB(
						NOW(),
						INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
					)
				) > 0 AS new'.(Combination::isFeatureActive() ? ',MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity' : '')
			.' FROM `'._DB_PREFIX_.'product` p
			'.Shop::addSqlAssociation('product', 'p').
			(Combination::isFeatureActive() ?
			'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (p.`id_product` = pa.`id_product`)
			'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1') : '').'
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
				ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
			LEFT JOIN `'._DB_PREFIX_.'image` i
				ON (i.`id_product` = p.`id_product`)'.
			Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il
				ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
				ON (m.`id_manufacturer` = p.`id_manufacturer`)
			'.Product::sqlStock('p', 0).
			($price_range && is_array($price_range) && isset($price_range['min'], $price_range['max']) ? ' LEFT OUTER JOIN '._DB_PREFIX_.'tax_rule tr ON (tr.id_tax_rules_group = p.id_tax_rules_group AND tr.id_country = '.pSQL($idCountry).') LEFT OUTER JOIN '._DB_PREFIX_.'tax t ON (t.id_tax = tr.id_tax AND t.active = 1)' : '').
			' INNER JOIN '._DB_PREFIX_.'specific_price psp ON psp.id_product = p.id_product ';

			if (Group::isFeatureActive() || $active_category || $current_category)
			{
				$sql_specific .= 'JOIN `'._DB_PREFIX_.'category_product` cp ON (p.id_product = cp.id_product)';
				if (Group::isFeatureActive())
					$sql_specific .= 'JOIN `'._DB_PREFIX_.'category_group` cg ON (cp.`id_category` = cg.`id_category` AND cg.`id_group` '.$sql_groups.')';
				if ($active_category)
					$sql_specific .= 'JOIN `'._DB_PREFIX_.'category` ca ON cp.`id_category` = ca.`id_category` AND ca.`active` = 1';
				if ($current_category)
					$sql_specific.= ' INNER JOIN `'._DB_PREFIX_.'category_product` cap ON (cap.`id_product` = p.`id_product` AND cap.`id_category` = '.pSQL((int) $current_category).')';
			}

			$sql_specific .= 
				($attribute_filter && trim($attribute_filter) != '' ?  ' INNER JOIN '._DB_PREFIX_.'product_attribute_combination pac ON pac.id_attribute IN('.pSQL($attribute_filter).')
				INNER JOIN '._DB_PREFIX_.'product_attribute ppa ON (ppa.id_product = p.id_product AND ppa.id_product_attribute = pac.id_product_attribute)' : '' ).
				($feature_filter && trim($feature_filter) != '' ? ' INNER JOIN '._DB_PREFIX_.'feature_product pfp ON (pfp.id_product = p.id_product AND pfp.id_feature_value IN('.pSQL($feature_filter).'))' : '').'
				WHERE p.`id_manufacturer` = '.(int)$id_manufacturer.'
				'.($active ? ' AND product_shop.`active` = 1' : '').'
				'.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
				$sQueryReductedPrice.'
				GROUP BY product_shop.id_product
				ORDER BY '.$alias.'`'.bqSQL($order_by).'` '.pSQL($order_way).'
				LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n.') ';
			
			$result_specific = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql_specific);
			if ($get_total)
			{
				$rTotal = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS("SELECT FOUND_ROWS() as fr;");
				if(isset($rTotal[0],$rTotal[0]['fr']))
					$iTotal += (int) $rTotal[0]['fr'];
			}
		}

		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		/* Return only the number of products */
		if ($get_total)
		{
			$rTotal = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS("SELECT FOUND_ROWS() as fr;");
			if(isset($rTotal[0],$rTotal[0]['fr']))
				$iTotal += (int) $rTotal[0]['fr'];
			return $iTotal;
		}

		if($price_range && $bUseSpecificPrice) {
			$result = array_merge_recursive($result, $result_specific);
		}

		if (!$result)
			return false;

		if ($order_by == 'price')
			Tools::orderbyPrice($result, $order_way);

		return Product::getProductsProperties($id_lang, $result);
	}
}