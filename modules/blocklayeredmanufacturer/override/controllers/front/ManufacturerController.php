<?php
/**
* 
* Manufacturer class overrider for blocklayeredmanufacturer plugin
* @date 2015-12-05
* @version 1.1.0
* @author Thomas GIORDMAINA <thomas.giordmaina@42webstreet.com>
* @copyright 42webstreet.com
*
*/
 
if(!function_exists('sanitize_attribute_array')) {
	function sanitize_attribute_array (&$tabItem, $key) {
		$tabItem = (int) $tabItem;
	}
}

class ManufacturerController extends ManufacturerControllerCore
{

}